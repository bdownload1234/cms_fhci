-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 29, 2022 at 06:37 AM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cms_database`
--

-- --------------------------------------------------------

--
-- Table structure for table `artikel`
--

CREATE TABLE `artikel` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `content` text DEFAULT NULL,
  `author` text DEFAULT NULL,
  `slug` text DEFAULT NULL,
  `meta_description` text NOT NULL,
  `thumbnail` text DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `artikel`
--

INSERT INTO `artikel` (`id`, `title`, `content`, `author`, `slug`, `meta_description`, `thumbnail`, `created_at`) VALUES
(13, 'Magang Bersertifikat', '<p>Konten</p>', NULL, NULL, '', '1655369580_c718d8884ef8e1ccef7c.jpg', '2022-06-15 17:00:00'),
(14, 'BUMN Muda rasa Start Up', '<p>Okay We All are gonna die</p>', NULL, NULL, '', '3-3.jpg', '2022-06-15 17:00:00'),
(15, 'Forum Serikat Kerja di Indonesia', '<div>Debout les damnés de la terre</div><div>Debout les forçats de la faim</div><div>La raison tonne en son cratère</div><div>C\'est l\'éruption de la faim</div><div>Du passé faisons table rase</div><div>Foule esclave debout debout</div><div>Le monde va changer de base</div><div>Nous ne sommes rien soyons tout</div>', NULL, NULL, '', 'export data to excel.PNG', '2022-06-15 17:00:00');
INSERT INTO `artikel` (`id`, `title`, `content`, `author`, `slug`, `meta_description`, `thumbnail`, `created_at`) VALUES
(19, 'Communist Manifesto', '<div>The Communist Manifesto</div><div>by Karl Marx and Friedrich Engels</div><div>[From the English edition of 1888, edited by Friedrich Engels]</div><div><br></div><div>Contents</div><div>I. BOURGEOIS AND PROLETARIANS</div><div>II. PROLETARIANS AND COMMUNISTS</div><div>III. SOCIALIST AND COMMUNIST LITERATURE</div><div>IV. POSITION OF THE COMMUNISTS IN RELATION TO THE VARIOUS EXISTING OPPOSITION PARTIES</div><div>A spectre is haunting Europe—the spectre of Communism. All the Powers of old Europe have entered into a holy alliance to exorcise this spectre: Pope and Czar, Metternich and Guizot, French Radicals and German police-spies.</div><div><br></div><div>Where is the party in opposition that has not been decried as Communistic by its opponents in power? Where is the Opposition that has not hurled back the branding reproach of Communism, against the more advanced opposition parties, as well as against its reactionary adversaries?</div><div><br></div><div>Two things result from this fact.</div><div><br></div><div>I. Communism is already acknowledged by all European Powers to be itself a Power.</div><div><br></div><div>II. It is high time that Communists should openly, in the face of the whole world, publish their views, their aims, their tendencies, and meet this nursery tale of the Spectre of Communism with a Manifesto of the party itself.</div><div><br></div><div>To this end, Communists of various nationalities have assembled in London, and sketched the following Manifesto, to be published in the English, French, German, Italian, Flemish and Danish languages.</div><div><br></div><div>I.</div><div>BOURGEOIS AND PROLETARIANS</div><div>The history of all hitherto existing societies is the history of class struggles.</div><div><br></div><div>Freeman and slave, patrician and plebeian, lord and serf, guild-master and journeyman, in a word, oppressor and oppressed, stood in constant opposition to one another, carried on an uninterrupted, now hidden, now open fight, a fight that each time ended, either in a revolutionary re-constitution of society at large, or in the common ruin of the contending classes.</div><div><br></div><div>In the earlier epochs of history, we find almost everywhere a complicated arrangement of society into various orders, a manifold gradation of social rank. In ancient Rome we have patricians, knights, plebeians, slaves; in the Middle Ages, feudal lords, vassals, guild-masters, journeymen, apprentices, serfs; in almost all of these classes, again, subordinate gradations.</div><div><br></div><div>The modern bourgeois society that has sprouted from the ruins of feudal society has not done away with class antagonisms. It has but established new classes, new conditions of oppression, new forms of struggle in place of the old ones. Our epoch, the epoch of the bourgeoisie, possesses, however, this distinctive feature: it has simplified the class antagonisms. Society as a whole is more and more splitting up into two great hostile camps, into two great classes, directly facing each other: Bourgeoisie and Proletariat.</div><div><br></div><div>From the serfs of the Middle Ages sprang the chartered burghers of the earliest towns. From these burgesses the first elements of the bourgeoisie were developed.</div><div><br></div><div>The discovery of America, the rounding of the Cape, opened up fresh ground for the rising bourgeoisie. The East-Indian and Chinese markets, the colonisation of America, trade with the colonies, the increase in the means of exchange and in commodities generally, gave to commerce, to navigation, to industry, an impulse never before known, and thereby, to the revolutionary element in the tottering feudal society, a rapid development.</div><div><br></div><div>The feudal system of industry, under which industrial production was monopolised by closed guilds, now no longer sufficed for the growing wants of the new markets. The manufacturing system took its place. The guild-masters were pushed on one side by the manufacturing middle class; division of labour between the different corporate guilds vanished in the face of division of labour in each single workshop.</div><div><br></div><div>Meantime the markets kept ever growing, the demand ever rising. Even manufacture no longer sufficed. Thereupon, steam and machinery revolutionised industrial production. The place of manufacture was taken by the giant, Modern Industry, the place of the industrial middle class, by industrial millionaires, the leaders of whole industrial armies, the modern bourgeois.</div><div><br></div><div>Modern industry has established the world-market, for which the discovery of America paved the way. This market has given an immense development to commerce, to navigation, to communication by land. This development has, in its time, reacted on the extension of industry; and in proportion as industry, commerce, navigation, railways extended, in the same proportion the bourgeoisie developed, increased its capital, and pushed into the background every class handed down from the Middle Ages.</div><div><br></div><div>We see, therefore, how the modern bourgeoisie is itself the product of a long course of development, of a series of revolutions in the modes of production and of exchange.</div><div><br></div><div>Each step in the development of the bourgeoisie was accompanied by a corresponding political advance of that class. An oppressed class under the sway of the feudal nobility, an armed and self-governing association in the mediaeval commune; here independent urban republic (as in Italy and Germany), there taxable “third estate” of the monarchy (as in France), afterwards, in the period of manufacture proper, serving either the semi-feudal or the absolute monarchy as a counterpoise against the nobility, and, in fact, corner-stone of the great monarchies in general, the bourgeoisie has at last, since the establishment of Modern Industry and of the world-market, conquered for itself, in the modern representative State, exclusive political sway. The executive of the modern State is but a committee for managing the common affairs of the whole bourgeoisie.</div><div><br></div><div>The bourgeoisie, historically, has played a most revolutionary part.</div><div><br></div><div>The bourgeoisie, wherever it has got the upper hand, has put an end to all feudal, patriarchal, idyllic relations. It has pitilessly torn asunder the motley feudal ties that bound man to his “natural superiors,” and has left remaining no other nexus between man and man than naked self-interest, than callous “cash payment.” It has drowned the most heavenly ecstasies of religious fervour, of chivalrous enthusiasm, of philistine sentimentalism, in the icy water of egotistical calculation. It has resolved personal worth into exchange value, and in place of the numberless and indefeasible chartered freedoms, has set up that single, unconscionable freedom—Free Trade. In one word, for exploitation, veiled by religious and political illusions, naked, shameless, direct, brutal exploitation.</div><div><br></div><div>The bourgeoisie has stripped of its halo every occupation hitherto honoured and looked up to with reverent awe. It has converted the physician, the lawyer, the priest, the poet, the man of science, into its paid wage labourers.</div><div><br></div><div>The bourgeoisie has torn away from the family its sentimental veil, and has reduced the family relation to a mere money relation.</div><div><br></div><div>The bourgeoisie has disclosed how it came to pass that the brutal display of vigour in the Middle Ages, which Reactionists so much admire, found its fitting complement in the most slothful indolence. It has been the first to show what man’s activity can bring about. It has accomplished wonders far surpassing Egyptian pyramids, Roman aqueducts, and Gothic cathedrals; it has conducted expeditions that put in the shade all former Exoduses of nations and crusades.</div><div><br></div><div>The bourgeoisie cannot exist without constantly revolutionising the instruments of production, and thereby the relations of production, and with them the whole relations of society. Conservation of the old modes of production in unaltered form, was, on the contrary, the first condition of existence for all earlier industrial classes. Constant revolutionising of production, uninterrupted disturbance of all social conditions, everlasting uncertainty and agitation distinguish the bourgeois epoch from all earlier ones. All fixed, fast-frozen relations, with their train of ancient and venerable prejudices and opinions, are swept away, all new-formed ones become antiquated before they can ossify. All that is solid melts into air, all that is holy is profaned, and man is at last compelled to face with sober senses, his real conditions of life, and his relations with his kind.</div><div><br></div><div>The need of a constantly expanding market for its products chases the bourgeoisie over the whole surface of the globe. It must nestle everywhere, settle everywhere, establish connexions everywhere.</div><div><br></div><div>The bourgeoisie has through its exploitation of the world-market given a cosmopolitan character to production and consumption in every country. To the great chagrin of Reactionists, it has drawn from under the feet of industry the national ground on which it stood. All old-established national industries have been destroyed or are daily being destroyed. They are dislodged by new industries, whose introduction becomes a life and death question for all civilised nations, by industries that no longer work up indigenous raw material, but raw material drawn from the remotest zones; industries whose products are consumed, not only at home, but in every quarter of the globe. In place of the old wants, satisfied by the productions of the country, we find new wants, requiring for their satisfaction the products of distant lands and climes. In place of the old local and national seclusion and self-sufficiency, we have intercourse in every direction, universal inter-dependence of nations. And as in material, so also in intellectual production. The intellectual creations of individual nations become common property. National one-sidedness and narrow-mindedness become more and more impossible, and from the numerous national and local literatures, there arises a world literature.</div><div><br></div><div>The bourgeoisie, by the rapid improvement of all instruments of production, by the immensely facilitated means of communication, draws all, even the most barbarian, nations into civilisation. The cheap prices of its commodities are the heavy artillery with which it batters down all Chinese walls, with which it forces the barbarians’ intensely obstinate hatred of foreigners to capitulate. It compels all nations, on pain of extinction, to adopt the bourgeois mode of production; it compels them to introduce what it calls civilisation into their midst, i.e., to become bourgeois themselves. In one word, it creates a world after its own image.</div><div><br></div><div>The bourgeoisie has subjected the country to the rule of the towns. It has created enormous cities, has greatly increased the urban population as compared with the rural, and has thus rescued a considerable part of the population from the idiocy of rural life. Just as it has made the country dependent on the towns, so it has made barbarian and semi-barbarian countries dependent on the civilised ones, nations of peasants on nations of bourgeois, the East on the West.</div><div><br></div><div>The bourgeoisie keeps more and more doing away with the scattered state of the population, of the means of production, and of property. It has agglomerated production, and has concentrated property in a few hands. The necessary consequence of this was political centralisation. Independent, or but loosely connected provinces, with separate interests, laws, governments and systems of taxation, became lumped together into one nation, with one government, one code of laws, one national class-interest, one frontier and one customs-tariff. The bourgeoisie, during its rule of scarce one hundred years, has created more massive and more colossal productive forces than have all preceding generations together. Subjection of Nature’s forces to man, machinery, application of chemistry to industry and agriculture, steam-navigation, railways, electric telegraphs, clearing of whole continents for cultivation, canalisation of rivers, whole populations conjured out of the ground—what earlier century had even a presentiment that such productive forces slumbered in the lap of social labour?</div><div><br></div><div>We see then: the means of production and of exchange, on whose foundation the bourgeoisie built itself up, were generated in feudal society. At a certain stage in the development of these means of production and of exchange, the conditions under which feudal society produced and exchanged, the feudal organisation of agriculture and manufacturing industry, in one word, the feudal relations of property became no longer compatible with the already developed productive forces; they became so many fetters. They had to be burst asunder; they were burst asunder.</div><div><br></div><div>Into their place stepped free competition, accompanied by a social and political constitution adapted to it, and by the economical and political sway of the bourgeois class.</div><div><br></div><div>A similar movement is going on before our own eyes. Modern bourgeois society with its relations of production, of exchange and of property, a society that has conjured up such gigantic means of production and of exchange, is like the sorcerer, who is no longer able to control the powers of the nether world whom he has called up by his spells. For many a decade past the history of industry and commerce is but the history of the revolt of modern productive forces against modern conditions of production, against the property relations that are the conditions for the existence of the bourgeoisie and of its rule. It is enough to mention the commercial crises that by their periodical return put on its trial, each time more threateningly, the existence of the entire bourgeois society. In these crises a great part not only of the existing products, but also of the previously created productive forces, are periodically destroyed. In these crises there breaks out an epidemic that, in all earlier epochs, would have seemed an absurdity—the epidemic of over-production. Society suddenly finds itself put back into a state of momentary barbarism; it appears as if a famine, a universal war of devastation had cut off the supply of every means of subsistence; industry and commerce seem to be destroyed; and why? Because there is too much civilisation, too much means of subsistence, too much industry, too much commerce. The productive forces at the disposal of society no longer tend to further the development of the conditions of bourgeois property; on the contrary, they have become too powerful for these conditions, by which they are fettered, and so soon as they overcome these fetters, they bring disorder into the whole of bourgeois society, endanger the existence of bourgeois property. The conditions of bourgeois society are too narrow to comprise the wealth created by them. And how does the bourgeoisie get over these crises? On the one hand inforced destruction of a mass of productive forces; on the other, by the conquest of new markets, and by the more thorough exploitation of the old ones. That is to say, by paving the way for more extensive and more destructive crises, and by diminishing the means whereby crises are prevented.</div><div><br></div><div>The weapons with which the bourgeoisie felled feudalism to the ground are now turned against the bourgeoisie itself.</div><div><br></div><div>But not only has the bourgeoisie forged the weapons that bring death to itself; it has also called into existence the men who are to wield those weapons—the modern working class—the proletarians.</div><div><br></div><div>In proportion as the bourgeoisie, i.e., capital, is developed, in the same proportion is the proletariat, the modern working class, developed—a class of labourers, who live only so long as they find work, and who find work only so long as their labour increases capital. These labourers, who must sell themselves piece-meal, are a commodity, like every other article of commerce, and are consequently exposed to all the vicissitudes of competition, to all the fluctuations of the market.</div><div><br></div><div>Owing to the extensive use of machinery and to division of labour, the work of the proletarians has lost all individual character, and consequently, all charm for the workman. He becomes an appendage of the machine, and it is only the most simple, most monotonous, and most easily acquired knack, that is required of him. Hence, the cost of production of a workman is restricted, almost entirely, to the means of subsistence that he requires for his maintenance, and for the propagation of his race. But the price of a commodity, and therefore also of labour, is equal to its cost of production. In proportion therefore, as the repulsiveness of the work increases, the wage decreases. Nay more, in proportion as the use of machinery and division of labour increases, in the same proportion the burden of toil also increases, whether by prolongation of the working hours, by increase of the work exacted in a given time or by increased speed of the machinery, etc.</div><div><br></div><div>Modern industry has converted the little workshop of the patriarchal master into the great factory of the industrial capitalist. Masses of labourers, crowded into the factory, are organised like soldiers. As privates of the industrial army they are placed under the command of a perfect hierarchy of officers and sergeants. Not only are they slaves of the bourgeois class, and of the bourgeois State; they are daily and hourly enslaved by the machine, by the over-looker, and, above all, by the individual bourgeois manufacturer himself. The more openly this despotism proclaims gain to be its end and aim, the more petty, the more hateful and the more embittering it is.</div><div><br></div><div>The less the skill and exertion of strength implied in manual labour, in other words, the more modern industry becomes developed, the more is the labour of men superseded by that of women. Differences of age and sex have no longer any distinctive social validity for the working class. All are instruments of labour, more or less expensive to use, according to their age and sex.</div><div><br></div><div>No sooner is the exploitation of the labourer by the manufacturer, so far at an end, that he receives his wages in cash, than he is set upon by the other portions of the bourgeoisie, the landlord, the shopkeeper, the pawnbroker, etc.</div><div><br></div><div>The lower strata of the middle class—the small tradespeople, shopkeepers, retired tradesmen generally, the handicraftsmen and peasants—all these sink gradually into the proletariat, partly because their diminutive capital does not suffice for the scale on which Modern Industry is carried on, and is swamped in the competition with the large capitalists, partly because their specialized skill is rendered worthless by the new methods of production. Thus the proletariat is recruited from all classes of the population.</div><div><br></div><div>The proletariat goes through various stages of development. With its birth begins its struggle with the bourgeoisie. At first the contest is carried on by individual labourers, then by the workpeople of a factory, then by the operatives of one trade, in one locality, against the individual bourgeois who directly exploits them. They direct their attacks not against the bourgeois conditions of production, but against the instruments of production themselves; they destroy imported wares that compete with their labour, they smash to pieces machinery, they set factories ablaze, they seek to restore by force the vanished status of the workman of the Middle Ages.</div><div><br></div><div>At this stage the labourers still form an incoherent mass scattered over the whole country, and broken up by their mutual competition. If anywhere they unite to form more compact bodies, this is not yet the consequence of their own active union, but of the union of the bourgeoisie, which class, in order to attain its own political ends, is compelled to set the whole proletariat in motion, and is moreover yet, for a time, able to do so. At this stage, therefore, the proletarians do not fight their enemies, but the enemies of their enemies, the remnants of absolute monarchy, the landowners, the non-industrial bourgeois, the petty bourgeoisie. Thus the whole historical movement is concentrated in the hands of the bourgeoisie; every victory so obtained is a victory for the bourgeoisie.</div><div><br></div><div>But with the development of industry the proletariat not only increases in number; it becomes concentrated in greater masses, its strength grows, and it feels that strength more. The various interests and conditions of life within the ranks of the proletariat are more and more equalised, in proportion as machinery obliterates all distinctions of labour, and nearly everywhere reduces wages to the same low level. The growing competition among the bourgeois, and the resulting commercial crises, make the wages of the workers ever more fluctuating. The unceasing improvement of machinery, ever more rapidly developing, makes their livelihood more and more precarious; the collisions between individual workmen and individual bourgeois take more and more the character of collisions between two classes. Thereupon the workers begin to form combinations (Trades Unions) against the bourgeois; they club together in order to keep up the rate of wages; they found permanent associations in order to make provision beforehand for these occasional revolts. Here and there the contest breaks out into riots.</div><div><br></div><div>Now and then the workers are victorious, but only for a time. The real fruit of their battles lies, not in the immediate result, but in the ever-expanding union of the workers. This union is helped on by the improved means of communication that are created by modern industry and that place the workers of different localities in contact with one another. It was just this contact that was needed to centralise the numerous local struggles, all of the same character, into one national struggle between classes. But every class struggle is a political struggle. And that union, to attain which the burghers of the Middle Ages, with their miserable highways, required centuries, the modern proletarians, thanks to railways, achieve in a few years.</div><div><br></div><div>This organisation of the proletarians into a class, and consequently into a political party, is continually being upset again by the competition between the workers themselves. But it ever rises up again, stronger, firmer, mightier. It compels legislative recognition of particular interests of the workers, by taking advantage of the divisions among the bourgeoisie itself. Thus the ten-hours’ bill in England was carried.</div><div><br></div><div>Altogether collisions between the classes of the old society further, in many ways, the course of development of the proletariat. The bourgeoisie finds itself involved in a constant battle. At first with the aristocracy; later on, with those portions of the bourgeoisie itself, whose interests have become antagonistic to the progress of industry; at all times, with the bourgeoisie of foreign countries. In all these battles it sees itself compelled to appeal to the proletariat, to ask for its help, and thus, to drag it into the political arena. The bourgeoisie itself, therefore, supplies the proletariat with its own instruments of political and general education, in other words, it furnishes the proletariat with weapons for fighting the bourgeoisie.</div><div><br></div><div>Further, as we have already seen, entire sections of the ruling classes are, by the advance of industry, precipitated into the proletariat, or are at least threatened in their conditions of existence. These also supply the proletariat with fresh elements of enlightenment and progress.</div><div><br></div><div>Finally, in times when the class struggle nears the decisive hour, the process of dissolution going on within the ruling class, in fact within the whole range of society, assumes such a violent, glaring character, that a small section of the ruling class cuts itself adrift, and joins the revolutionary class, the class that holds the future in its hands. Just as, therefore, at an earlier period, a section of the nobility went over to the bourgeoisie, so now a portion of the bourgeoisie goes over to the proletariat, and in particular, a portion of the bourgeois ideologists, who have raised themselves to the level of comprehending theoretically the historical movement as a whole.</div><div><br></div><div>Of all the classes that stand face to face with the bourgeoisie today, the proletariat alone is a really revolutionary class. The other classes decay and finally disappear in the face of Modern Industry; the proletariat is its special and essential product. The lower middle class, the small manufacturer, the shopkeeper, the artisan, the peasant, all these fight against the bourgeoisie, to save from extinction their existence as fractions of the middle class. They are therefore not revolutionary, but conservative. Nay more, they are reactionary, for they try to roll back the wheel of history. If by chance they are revolutionary, they are so only in view of their impending transfer into the proletariat, they thus defend not their present, but their future interests, they desert their own standpoint to place themselves at that of the proletariat.</div><div><br></div><div>The “dangerous class,” the social scum, that passively rotting mass thrown off by the lowest layers of old society, may, here and there, be swept into the movement by a proletarian revolution; its conditions of life, however, prepare it far more for the part of a bribed tool of reactionary intrigue.</div><div><br></div><div>In the conditions of the proletariat, those of old society at large are already virtually swamped. The proletarian is without property; his relation to his wife and children has no longer anything in common with the bourgeois family-relations; modern industrial labour, modern subjection to capital, the same in England as in France, in America as in Germany, has stripped him of every trace of national character. Law, morality, religion, are to him so many bourgeois prejudices, behind which lurk in ambush just as many bourgeois interests.</div><div><br></div><div>All the preceding classes that got the upper hand, sought to fortify their already acquired status by subjecting society at large to their conditions of appropriation. The proletarians cannot become masters of the productive forces of society, except by abolishing their own previous mode of appropriation, and thereby also every other previous mode of appropriation. They have nothing of their own to secure and to fortify; their mission is to destroy all previous securities for, and insurances of, individual property.</div><div><br></div><div>All previous historical movements were movements of minorities, or in the interests of minorities. The proletarian movement is the self-conscious, independent movement of the immense majority, in the interests of the immense majority. The proletariat, the lowest stratum of our present society, cannot stir, cannot raise itself up, without the whole superincumbent strata of official society being sprung into the air.</div><div><br></div><div>Though not in substance, yet in form, the struggle of the proletariat with the bourgeoisie is at first a national struggle. The proletariat of each country must, of course, first of all settle matters with its own bourgeoisie.</div><div><br></div><div>In depicting the most general phases of the development of the proletariat, we traced the more or less veiled civil war, raging within existing society, up to the point where that war breaks out into open revolution, and where the violent overthrow of the bourgeoisie lays the foundation for the sway of the proletariat.</div><div><br></div><div>Hitherto, every form of society has been based, as we have already seen, on the antagonism of oppressing and oppressed classes. But in order to oppress a class, certain conditions must be assured to it under which it can, at least, continue its slavish existence. The serf, in the period of serfdom, raised himself to membership in the commune, just as the petty bourgeois, under the yoke of feudal absolutism, managed to develop into a bourgeois. The modern laborer, on the contrary, instead of rising with the progress of industry, sinks deeper and deeper below the conditions of existence of his own class. He becomes a pauper, and pauperism develops more rapidly than population and wealth. And here it becomes evident, that the bourgeoisie is unfit any longer to be the ruling class in society, and to impose its conditions of existence upon society as an over-riding law. It is unfit to rule because it is incompetent to assure an existence to its slave within his slavery, because it cannot help letting him sink into such a state, that it has to feed him, instead of being fed by him. Society can no longer live under this bourgeoisie, in other words, its existence is no longer compatible with society.</div><div><br></div><div>The essential condition for the existence, and for the sway of the bourgeois class, is the formation and augmentation of capital; the condition for capital is wage-labour. Wage-labour rests exclusively on competition between the laborers. The advance of industry, whose involuntary promoter is the bourgeoisie, replaces the isolation of the labourers, due to competition, by their revolutionary combination, due to association. The development of Modern Industry, therefore, cuts from under its feet the very foundation on which the bourgeoisie produces and appropriates products. What the bourgeoisie, therefore, produces, above all, is its own grave-diggers. Its fall and the victory of the proletariat are equally inevitable.</div><div><br></div><div>II.</div><div>PROLETARIANS AND COMMUNISTS</div><div>In what relation do the Communists stand to the proletarians as a whole?</div><div><br></div><div>The Communists do not form a separate party opposed to other working-class parties.</div><div><br></div><div>They have no interests separate and apart from those of the proletariat as a whole.</div><div><br></div><div>They do not set up any sectarian principles of their own, by which to shape and mould the proletarian movement.</div><div><br></div><div>The Communists are distinguished from the other working-class parties by this only: (1) In the national struggles of the proletarians of the different countries, they point out and bring to the front the common interests of the entire proletariat, independently of all nationality. (2) In the various stages of development which the struggle of the working class against the bourgeoisie has to pass through, they always and everywhere represent the interests of the movement as a whole.</div><div><br></div><div>The Communists, therefore, are on the one hand, practically, the most advanced and resolute section of the working-class parties of every country, that section which pushes forward all others; on the other hand, theoretically, they have over the great mass of the proletariat the advantage of clearly understanding the line of march, the conditions, and the ultimate general results of the proletarian movement.</div><div><br></div><div>The immediate aim of the Communist is the same as that of all the other proletarian parties: formation of the proletariat into a class, overthrow of the bourgeois supremacy, conquest of political power by the proletariat.</div><div><br></div><div>The theoretical conclusions of the Communists are in no way based on ideas or principles that have been invented, or discovered, by this or that would-be universal reformer. They merely express, in general terms, actual relations springing from an existing class struggle, from a historical movement going on under our very eyes. The abolition of existing property relations is not at all a distinctive feature of Communism.</div><div><br></div><div>All property relations in the past have continually been subject to historical change consequent upon the change in historical conditions.</div><div><br></div><div>The French Revolution, for example, abolished feudal property in favour of bourgeois property.</div><div><br></div><div>The distinguishing feature of Communism is not the abolition of property generally, but the abolition of bourgeois property. But modern bourgeois private property is the final and most complete expression of the system of producing and appropriating products, that is based on class antagonisms, on the exploitation of the many by the few.</div><div><br></div><div>In this sense, the theory of the Communists may be summed up in the single sentence: Abolition of private property.</div><div><br></div><div>We Communists have been reproached with the desire of abolishing the right of personally acquiring property as the fruit of a man’s own labour, which property is alleged to be the groundwork of all personal freedom, activity and independence.</div><div><br></div><div>Hard-won, self-acquired, self-earned property! Do you mean the property of the petty artisan and of the small peasant, a form of property that preceded the bourgeois form? There is no need to abolish that; the development of industry has to a great extent already destroyed it, and is still destroying it daily.</div><div><br></div><div>Or do you mean modern bourgeois private property?</div><div><br></div><div>But does wage-labour create any property for the labourer? Not a bit. It creates capital, i.e., that kind of property which exploits wage-labour, and which cannot increase except upon condition of begetting a new supply of wage-labour for fresh exploitation. Property, in its present form, is based on the antagonism of capital and wage-labour. Let us examine both sides of this antagonism.</div><div><br></div><div>To be a capitalist, is to have not only a purely personal, but a social status in production. Capital is a collective product, and only by the united action of many members, nay, in the last resort, only by the united action of all members of society, can it be set in motion.</div><div><br></div><div>Capital is, therefore, not a personal, it is a social power.</div><div><br></div><div>When, therefore, capital is converted into common property, into the property of all members of society, personal property is not thereby transformed into social property. It is only the social character of the property that is changed. It loses its class-character.</div><div><br></div><div>Let us now take wage-labour.</div><div><br></div><div>The average price of wage-labour is the minimum wage, i.e., that quantum of the means of subsistence, which is absolutely requisite in bare existence as a labourer. What, therefore, the wage-labourer appropriates by means of his labour, merely suffices to prolong and reproduce a bare existence. We by no means intend to abolish this personal appropriation of the products of labour, an appropriation that is made for the maintenance and reproduction of human life, and that leaves no surplus wherewith to command the labour of others. All that we want to do away with, is the miserable character of this appropriation, under which the labourer lives merely to increase capital, and is allowed to live only in so far as the interest of the ruling class requires it.</div><div><br></div><div>In bourgeois society, living labour is but a means to increase accumulated labour. In Communist society, accumulated labour is but a means to widen, to enrich, to promote the existence of the labourer.</div><div><br></div><div>In bourgeois society, therefore, the past dominates the present; in Communist society, the present dominates the past. In bourgeois society capital is independent and has individuality, while the living person is dependent and has no individuality.</div><div><br></div><div>And the abolition of this state of things is called by the bourgeois, abolition of individuality and freedom! And rightly so. The abolition of bourgeois individuality, bourgeois independence, and bourgeois freedom is undoubtedly aimed at.</div><div><br></div><div>By freedom is meant, under the present bourgeois conditions of production, free trade, free selling and buying.</div><div><br></div><div>But if selling and buying disappears, free selling and buying disappears also. This talk about free selling and buying, and all the other “brave words” of our bourgeoisie about freedom in general, have a meaning, if any, only in contrast with restricted selling and buying, with the fettered traders of the Middle Ages, but have no meaning when opposed to the Communistic abolition of buying and selling, of the bourgeois conditions of production, and of the bourgeoisie itself.</div><div><br></div><div>You are horrified at our intending to do away with private property. But in your existing society, private property is already done away with for nine-tenths of the population; its existence for the few is solely due to its non-existence in the hands of those nine-tenths. You reproach us, therefore, with intending to do away with a form of property, the necessary condition for whose existence is the non-existence of any property for the immense majority of society.</div><div><br></div><div>In one word, you reproach us with intending to do away with your property. Precisely so; that is just what we intend.</div><div><br></div><div>From the moment when labour can no longer be converted into capital, money, or rent, into a social power capable of being monopolised, i.e., from the moment when individual property can no longer be transformed into bourgeois property, into capital, from that moment, you say individuality vanishes.</div><div><br></div><div>You must, therefore, confess that by “individual” you mean no other person than the bourgeois, than the middle-class owner of property. This person must, indeed, be swept out of the way, and made impossible.</div><div><br></div><div>Communism deprives no man of the power to appropriate the products of society; all that it does is to deprive him of the power to subjugate the labour of others by means of such appropriation.</div><div><br></div><div>It has been objected that upon the abolition of private property all work will cease, and universal laziness will overtake us.</div><div><br></div><div>According to this, bourgeois society ought long ago to have gone to the dogs through sheer idleness; for those of its members who work, acquire nothing, and those who acquire anything, do not work. The whole of this objection is but another expression of the tautology: that there can no longer be any wage-labour when there is no longer any capital.</div><div><br></div><div>All objections urged against the Communistic mode of producing and appropriating material products, have, in the same way, been urged against the Communistic modes of producing and appropriating intellectual products. Just as, to the bourgeois, the disappearance of class property is the disappearance of production itself, so the disappearance of class culture is to him identical with the disappearance of all culture.</div><div><br></div><div>That culture, the loss of which he laments, is, for the enormous majority, a mere training to act as a machine.</div><div><br></div><div>But don’t wrangle with us so long as you apply, to our intended abolition of bourgeois property, the standard of your bourgeois notions of freedom, culture, law, etc. Your very ideas are but the outgrowth of the conditions of your bourgeois production and bourgeois property, just as your jurisprudence is but the will of your class made into a law for all, a will, whose essential character and direction are determined by the economical conditions of existence of your class.</div><div><br></div><div>The selfish misconception that induces you to transform into eternal laws of nature and of reason, the social forms springing from your present mode of production and form of property—historical relations that rise and disappear in the progress of production—this misconception you share with every ruling class that has preceded you. What you see clearly in the case of ancient property, what you admit in the case of feudal property, you are of course forbidden to admit in the case of your own bourgeois form of property.</div><div><br></div><div>Abolition of the family! Even the most radical flare up at this infamous proposal of the Communists.</div><div><br></div><div>On what foundation is the present family, the bourgeois family, based? On capital, on private gain. In its completely developed form this family exists only among the bourgeoisie. But this state of things finds its complement in the practical absence of the family among the proletarians, and in public prostitution.</div><div><br></div><div>The bourgeois family will vanish as a matter of course when its complement vanishes, and both will vanish with the vanishing of capital.</div><div><br></div><div>Do you charge us with wanting to stop the exploitation of children by their parents? To this crime we plead guilty.</div><div><br></div><div>But, you will say, we destroy the most hallowed of relations, when we replace home education by social.</div><div><br></div><div>And your education! Is not that also social, and determined by the social conditions under which you educate, by the intervention, direct or indirect, of society, by means of schools, etc.? The Communists have not invented the intervention of society in education; they do but seek to alter the character of that intervention, and to rescue education from the influence of the ruling class.</div><div><br></div><div>The bourgeois clap-trap about the family and education, about the hallowed co-relation of parent and child, becomes all the more disgusting, the more, by the action of Modern Industry, all family ties among the proletarians are torn asunder, and their children transformed into simple articles of commerce and instruments of labour.</div><div><br></div><div>But you Communists would introduce community of women, screams the whole bourgeoisie in chorus.</div><div><br></div><div>The bourgeois sees in his wife a mere instrument of production. He hears that the instruments of production are to be exploited in common, and, naturally, can come to no other conclusion than that the lot of being common to all will likewise fall to the women.</div><div><br></div><div>He has not even a suspicion that the real point is to do away with the status of women as mere instruments of production.</div><div><br></div><div>For the rest, nothing is more ridiculous than the virtuous indignation of our bourgeois at the community of women which, they pretend, is to be openly and officially established by the Communists. The Communists have no need to introduce community of women; it has existed almost from time immemorial.</div><div><br></div><div>Our bourgeois, not content with having the wives and daughters of their proletarians at their disposal, not to speak of common prostitutes, take the greatest pleasure in seducing each other’s wives.</div><div><br></div><div>Bourgeois marriage is in reality a system of wives in common and thus, at the most, what the Communists might possibly be reproached with, is that they desire to introduce, in substitution for a hypocritically concealed, an openly legalised community of women. For the rest, it is self-evident that the abolition of the present system of production must bring with it the abolition of the community of women springing from that system, i.e., of prostitution both public and private.</div><div><br></div><div>The Communists are further reproached with desiring to abolish countries and nationality.</div><div><br></div><div>The working men have no country. We cannot take from them what they have not got. Since the proletariat must first of all acquire political supremacy, must rise to be the leading class of the nation, must constitute itself the nation, it is, so far, itself national, though not in the bourgeois sense of the word.</div><div><br></div><div>National differences and antagonisms between peoples are daily more and more vanishing, owing to the development of the bourgeoisie, to freedom of commerce, to the world-market, to uniformity in the mode of production and in the conditions of life corresponding thereto.</div><div><br></div><div>The supremacy of the proletariat will cause them to vanish still faster. United action, of the leading civilised countries at least, is one of the first conditions for the emancipation of the proletariat.</div><div><br></div><div>In proportion as the exploitation of one individual by another is put an end to, the exploitation of one nation by another will also be put an end to. In proportion as the antagonism between classes within the nation vanishes, the hostility of one nation to another will come to an end.</div><div><br></div><div>The charges against Communism made from a religious, a philosophical, and, generally, from an ideological standpoint, are not deserving of serious examination.</div><div><br></div><div>Does it require deep intuition to comprehend that man’s ideas, views and conceptions, in one word, man’s consciousness, changes with every change in the conditions of his material existence, in his social relations and in his social life?</div><div><br></div><div>What else does the history of ideas prove, than that intellectual production changes its character in proportion as material production is changed? The ruling ideas of each age have ever been the ideas of its ruling class.</div><div><br></div><div>When people speak of ideas that revolutionise society, they do but express the fact, that within the old society, the elements of a new one have been created, and that the dissolution of the old ideas keeps even pace with the dissolution of the old conditions of existence.</div><div><br></div><div>When the ancient world was in its last throes, the ancient religions were overcome by Christianity. When Christian ideas succumbed in the 18th century to rationalist ideas, feudal society fought its death battle with the then revolutionary bourgeoisie. The ideas of religious liberty and freedom of conscience merely gave expression to the sway of free competition within the domain of knowledge.</div><div><br></div><div>“Undoubtedly,” it will be said, “religious, moral, philosophical and juridical ideas have been modified in the course of historical development. But religion, morality philosophy, political science, and law, constantly survived this change.”</div><div><br></div><div>“There are, besides, eternal truths, such as Freedom, Justice, etc. that are common to all states of society. But Communism abolishes eternal truths, it abolishes all religion, and all morality, instead of constituting them on a new basis; it therefore acts in contradiction to all past historical experience.”</div><div><br></div><div>What does this accusation reduce itself to? The history of all past society has consisted in the development of class antagonisms, antagonisms that assumed different forms at different epochs.</div><div><br></div><div>But whatever form they may have taken, one fact is common to all past ages, viz., the exploitation of one part of society by the other. No wonder, then, that the social consciousness of past ages, despite all the multiplicity and variety it displays, moves within certain common forms, or general ideas, which cannot completely vanish except with the total disappearance of class antagonisms.</div><div><br></div><div>The Communist revolution is the most radical rupture with traditional property relations; no wonder that its development involves the most radical rupture with traditional ideas.</div><div><br></div><div>But let us have done with the bourgeois objections to Communism.</div><div><br></div><div>We have seen above, that the first step in the revolution by the working class, is to raise the proletariat to the position of ruling as to win the battle of democracy.</div><div><br></div><div>The proletariat will use its political supremacy to wrest, by degrees, all capital from the bourgeoisie, to centralise all instruments of production in the hands of the State, i.e., of the proletariat organised as the ruling class; and to increase the total of productive forces as rapidly as possible.</div><div><br></div><div>Of course, in the beginning, this cannot be effected except by means of despotic inroads on the rights of property, and on the conditions of bourgeois production; by means of measures, therefore, which appear economically insufficient and untenable, but which, in the course of the movement, outstrip themselves, necessitate further inroads upon the old social order, and are unavoidable as a means of entirely revolutionising the mode of production.</div><div><br></div><div>These measures will of course be different in different countries.</div><div><br></div><div>Nevertheless in the most advanced countries, the following will be pretty generally applicable.</div><div><br></div><div>1. Abolition of property in land and application of all rents of land to public purposes.</div><div><br></div><div>2. A heavy progressive or graduated income tax.</div><div><br></div><div>3. Abolition of all right of inheritance.</div><div><br></div><div>4. Confiscation of the property of all emigrants and rebels.</div><div><br></div><div>5. Centralisation of credit in the hands of the State, by means of a national bank with State capital and an exclusive monopoly.</div><div><br></div><div>6. Centralisation of the means of communication and transport in the hands of the State.</div><div><br></div><div>7. Extension of factories and instruments of production owned by the State; the bringing into cultivation of waste-lands, and the improvement of the soil generally in accordance with a common plan.</div><div><br></div><div>8. Equal liability of all to labour. Establishment of industrial armies, especially for agriculture.</div><div><br></div><div>9. Combination of agriculture with manufacturing industries; gradual abolition of the distinction between town and country, by a more equable distribution of the population over the country.</div><div><br></div><div>10. Free education for all children in public schools.&nbsp; &nbsp; &nbsp;Abolition of children’s factory labour in its present form.&nbsp; &nbsp; &nbsp;Combination of education with industrial production, &amp;c., &amp;c.</div><div><br></div><div>When, in the course of development, class distinctions have disappeared, and all production has been concentrated in the hands of a vast association of the whole nation, the public power will lose its political character. Political power, properly so called, is merely the organised power of one class for oppressing another. If the proletariat during its contest with the bourgeoisie is compelled, by the force of circumstances, to organise itself as a class, if, by means of a revolution, it makes itself the ruling class, and, as such, sweeps away by force the old conditions of production, then it will, along with these conditions, have swept away the conditions for the existence of class antagonisms and of classes generally, and will thereby have abolished its own supremacy as a class.</div><div><br></div><div>In place of the old bourgeois society, with its classes and class antagonisms, we shall have an association, in which the free development of each is the condition for the free development of all.</div><div><br></div><div>III.</div><div>SOCIALIST AND COMMUNIST LITERATURE</div><div>1. REACTIONARY SOCIALISM</div><div>A. Feudal Socialism</div><div><br></div><div>Owing to their historical position, it became the vocation of the aristocracies of France and England to write pamphlets against modern bourgeois society. In the French revolution of July 1830, and in the English reform agitation, these aristocracies again succumbed to the hateful upstart. Thenceforth, a serious political contest was altogether out of the question. A literary battle alone remained possible. But even in the domain of literature the old cries of the restoration period had become impossible.</div><div><br></div><div>In order to arouse sympathy, the aristocracy were obliged to lose sight, apparently, of their own interests, and to formulate their indictment against the bourgeoisie in the interest of the exploited working class alone. Thus the aristocracy took their revenge by singing lampoons on their new master, and whispering in his ears sinister prophecies of coming catastrophe.</div><div><br></div><div>In this way arose Feudal Socialism: half lamentation, half lampoon; half echo of the past, half menace of the future; at times, by its bitter, witty and incisive criticism, striking the bourgeoisie to the very heart’s core; but always ludicrous in its effect, through total incapacity to comprehend the march of modern history.</div><div><br></div><div>The aristocracy, in order to rally the people to them, waved the proletarian alms-bag in front for a banner. But the people, so often as it joined them, saw on their hindquarters the old feudal coats of arms, and deserted with loud and irreverent laughter.</div><div><br></div><div>One section of the French Legitimists and “Young England” exhibited this spectacle.</div><div><br></div><div>In pointing out that their mode of exploitation was different to that of the bourgeoisie, the feudalists forget that they exploited under circumstances and conditions that were quite different, and that are now antiquated. In showing that, under their rule, the modern proletariat never existed, they forget that the modern bourgeoisie is the necessary offspring of their own form of society.</div><div><br></div><div>For the rest, so little do they conceal the reactionary character of their criticism that their chief accusation against the bourgeoisie amounts to this, that under the bourgeois regime a class is being developed, which is destined to cut up root and branch the old order of society.</div><div><br></div><div>What they upbraid the bourgeoisie with is not so much that it creates a proletariat, as that it creates a revolutionary proletariat.</div><div><br></div><div>In political practice, therefore, they join in all coercive measures against the working class; and in ordinary life, despite their high falutin phrases, they stoop to pick up the golden apples dropped from the tree of industry, and to barter truth, love, and honour for traffic in wool, beetroot-sugar, and potato spirits.</div><div><br></div><div>As the parson has ever gone hand in hand with the landlord, so has Clerical Socialism with Feudal Socialism.</div><div><br></div><div>Nothing is easier than to give Christian asceticism a Socialist tinge. Has not Christianity declaimed against private property, against marriage, against the State? Has it not preached in the place of these, charity and poverty, celibacy and mortification of the flesh, monastic life and Mother Church? Christian Socialism is but the holy water with which the priest consecrates the heart-burnings of the aristocrat.</div><div><br></div><div>B. Petty-Bourgeois Socialism</div><div><br></div><div>The feudal aristocracy was not the only class that was ruined by the bourgeoisie, not the only class whose conditions of existence pined and perished in the atmosphere of modern bourgeois society. The mediaeval burgesses and the small peasant proprietors were the precursors of the modern bourgeoisie. In those countries which are but little developed, industrially and commercially, these two classes still vegetate side by side with the rising bourgeoisie.</div><div><br></div><div>In countries where modern civilisation has become fully developed, a new class of petty bourgeois has been formed, fluctuating between proletariat and bourgeoisie and ever renewing itself as a supplementary part of bourgeois society. The individual members of this class, however, are being constantly hurled down into the proletariat by the action of competition, and, as modern industry develops, they even see the moment approaching when they will completely disappear as an independent section of modern society, to be replaced, in manufactures, agriculture and commerce, by overlookers, bailiffs and shopmen.</div><div><br></div><div>In countries like France, where the peasants constitute far more than half of the population, it was natural that writers who sided with the proletariat against the bourgeoisie, should use, in their criticism of the bourgeois regime, the standard of the peasant and petty bourgeois, and from the standpoint of these intermediate classes should take up the cudgels for the working class. Thus arose petty-bourgeois Socialism. Sismondi was the head of this school, not only in France but also in England.</div><div><br></div><div>This school of Socialism dissected with great acuteness the contradictions in the conditions of modern production. It laid bare the hypocritical apologies of economists. It proved, incontrovertibly, the disastrous effects of machinery and division of labour; the concentration of capital and land in a few hands; overproduction and crises; it pointed out the inevitable ruin of the petty bourgeois and peasant, the misery of the proletariat, the anarchy in production, the crying inequalities in the distribution of wealth, the industrial war of extermination between nations, the dissolution of old moral bonds, of the old family relations, of the old nationalities.</div><div><br></div><div>In its positive aims, however, this form of Socialism aspires either to restoring the old means of production and of exchange, and with them the old property relations, and the old society, or to cramping the modern means of production and of exchange, within the framework of the old property relations that have been, and were bound to be, exploded by those means. In either case, it is both reactionary and Utopian.</div><div><br></div><div>Its last words are: corporate guilds for manufacture, patriarchal relations in agriculture.</div><div><br></div><div>Ultimately, when stubborn historical facts had dispersed all intoxicating effects of self-deception, this form of Socialism ended in a miserable fit of the blues.</div><div><br></div><div>C. German, or “True,” Socialism</div><div><br></div><div>The Socialist and Communist literature of France, a literature that originated under the pressure of a bourgeoisie in power, and that was the expression of the struggle against this power, was introduced into Germany at a time when the bourgeoisie, in that country, had just begun its contest with feudal absolutism.</div><div><br></div><div>German philosophers, would-be philosophers, and beaux esprits, eagerly seized on this literature, only forgetting, that when these writings immigrated from France into Germany, French social conditions had not immigrated along with them. In contact with German social conditions, this French literature lost all its immediate practical significance, and assumed a purely literary aspect. Thus, to the German philosophers of the eighteenth century, the demands of the first French Revolution were nothing more than the demands of “Practical Reason” in general, and the utterance of the will of the revolutionary French bourgeoisie signified in their eyes the law of pure Will, of Will as it was bound to be, of true human Will generally.</div><div><br></div><div>The world of the German literati consisted solely in bringing the new French ideas into harmony with their ancient philosophical conscience, or rather, in annexing the French ideas without deserting their own philosophic point of view.</div><div><br></div><div>This annexation took place in the same way in which a foreign language is appropriated, namely, by translation.</div><div><br></div><div>It is well known how the monks wrote silly lives of Catholic Saints over the manuscripts on which the classical works of ancient heathendom had been written. The German literati reversed this process with the profane French literature. They wrote their philosophical nonsense beneath the French original. For instance, beneath the French criticism of the economic functions of money, they wrote “Alienation of Humanity,” and beneath the French criticism of the bourgeois State they wrote “dethronement of the Category of the General,” and so forth.</div><div><br></div><div>The introduction of these philosophical phrases at the back of the French historical criticisms they dubbed “Philosophy of Action,” “True Socialism,” “German Science of Socialism,” “Philosophical Foundation of Socialism,” and so on.</div><div><br></div><div>The French Socialist and Communist literature was thus completely emasculated. And, since it ceased in the hands of the German to express the struggle of one class with the other, he felt conscious of having overcome “French one-sidedness” and of representing, not true requirements, but the requirements of truth; not the interests of the proletariat, but the interests of Human Nature, of Man in general, who belongs to no class, has no reality, who exists only in the misty realm of philosophical fantasy.</div><div><br></div><div>This German Socialism, which took its schoolboy task so seriously and solemnly, and extolled its poor stock-in-trade in such mountebank fashion, meanwhile gradually lost its pedantic innocence.</div><div><br></div><div>The fight of the German, and especially, of the Prussian bourgeoisie, against feudal aristocracy and absolute monarchy, in other words, the liberal movement, became more earnest.</div><div><br></div><div>By this, the long wished-for opportunity was offered to “True” Socialism of confronting the political movement with the Socialist demands, of hurling the traditional anathemas against liberalism, against representative government, against bourgeois competition, bourgeois freedom of the press, bourgeois legislation, bourgeois liberty and equality, and of preaching to the masses that they had nothing to gain, and everything to lose, by this bourgeois movement. German Socialism forgot, in the nick of time, that the French criticism, whose silly echo it was, presupposed the existence of modern bourgeois society, with its corresponding economic conditions of existence, and the political constitution adapted thereto, the very things whose attainment was the object of the pending struggle in Germany.</div><div><br></div><div>To the absolute governments, with their following of parsons, professors, country squires and officials, it served as a welcome scarecrow against the threatening bourgeoisie.</div><div><br></div><div>It was a sweet finish after the bitter pills of floggings and bullets with which these same governments, just at that time, dosed the German working-class risings.</div><div><br></div><div>While this “True” Socialism thus served the governments as a weapon for fighting the German bourgeoisie, it, at the same time, directly represented a reactionary interest, the interest of the German Philistines. In Germany the petty bourgeois class, a relique of the sixteenth century, and since then constantly cropping up again under various forms, is the real social basis of the existing state of things.</div><div><br></div><div>To preserve this class is to preserve the existing state of things in Germany. The industrial and political supremacy of the bourgeoisie threatens it with certain destruction; on the one hand, from the concentration of capital; on the other, from the rise of a revolutionary proletariat. “True” Socialism appeared to kill these two birds with one stone. It spread like an epidemic.</div><div><br></div><div>The robe of speculative cobwebs, embroidered with flowers of rhetoric, steeped in the dew of sickly sentiment, this transcendental robe in which the German Socialists wrapped their sorry “eternal truths,” all skin and bone, served to wonderfully increase the sale of their goods amongst such a public. And on its part, German Socialism recognised, more and more, its own calling as the bombastic representative of the petty-bourgeois Philistine.</div><div><br></div><div>It proclaimed the German nation to be the model nation, and the German petty Philistine to be the typical man. To every villainous meanness of this model man it gave a hidden, higher, Socialistic interpretation, the exact contrary of its real character. It went to the extreme length of directly opposing the “brutally destructive” tendency of Communism, and of proclaiming its supreme and impartial contempt of all class str', NULL, NULL, '', '1655447413_1eeb3848cd6f5302c816.png', '2022-06-16 17:00:00');
INSERT INTO `artikel` (`id`, `title`, `content`, `author`, `slug`, `meta_description`, `thumbnail`, `created_at`) VALUES
(25, 'Contoh ada slugnya gak', '<p>slugs</p>', NULL, 'contohdashadadashslugnyadashgak', '', NULL, '2022-06-23 07:33:27'),
(26, 'harusnya bener kali ini', '<p>iya</p>', NULL, 'rusnyadashbenerdashkalidashini', '', NULL, '2022-06-23 07:34:16'),
(27, 'yang ini bener harusnya beneran', '<p>asdasdasd</p>', NULL, 'yang-ini-bener-harusnya-beneran', '', NULL, '2022-06-23 07:34:44');

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `id` int(11) NOT NULL,
  `title` text DEFAULT NULL,
  `thumbnail` text DEFAULT NULL,
  `slug` text DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`id`, `title`, `thumbnail`, `slug`, `created_at`) VALUES
(1, 'Home Banner 1', '1655430303_686e6209a4eb59ab0dc9.jpg', NULL, '2022-06-16 17:00:00'),
(4, 'Home Banner 2', '1655438652_8d88a5789021be46ec47.jpg', NULL, '2022-06-16 17:00:00'),
(8, 'Banner 2', '1655439802_97953ef5e72ff2394b26.png', NULL, '2022-06-16 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `banner_video`
--

CREATE TABLE `banner_video` (
  `id` int(11) NOT NULL,
  `title` text DEFAULT NULL,
  `video` text DEFAULT NULL,
  `slug` text DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `banner_video`
--

INSERT INTO `banner_video` (`id`, `title`, `video`, `slug`, `created_at`) VALUES
(5, 'Sheeeess', 'videoplayback (1).mp4', NULL, '2022-06-28 16:05:10');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `name` text DEFAULT NULL,
  `email` text DEFAULT NULL,
  `message` text DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `id` int(11) NOT NULL,
  `title` text DEFAULT NULL,
  `short_description` text DEFAULT NULL,
  `date` text DEFAULT NULL,
  `time_from` text DEFAULT NULL,
  `time_to` text DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`id`, `title`, `short_description`, `date`, `time_from`, `time_to`, `created_at`) VALUES
(1, 'Acara Internal Kantor', 'Acara 17 Agustus', '17-08-2022', '09:30', 'Selesai', '2022-06-20 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL,
  `caption` text DEFAULT NULL,
  `thumbnail` text DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `press`
--

CREATE TABLE `press` (
  `id` int(11) NOT NULL,
  `title` text DEFAULT NULL,
  `content` text DEFAULT NULL,
  `author` text DEFAULT NULL,
  `slug` text DEFAULT NULL,
  `meta_description` text DEFAULT NULL,
  `thumbnail` text DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `press`
--

INSERT INTO `press` (`id`, `title`, `content`, `author`, `slug`, `meta_description`, `thumbnail`, `created_at`) VALUES
(2, 'Test', '<p>Apasaja mengenai hal ini</p>', 'superadmin1234', 'test', 'Apasaja mengenai hal ini', '1656473422_509db86c3ad594e0945f.jpg', '2022-06-29 10:30:22');

-- --------------------------------------------------------

--
-- Table structure for table `press_comment`
--

CREATE TABLE `press_comment` (
  `id` int(11) NOT NULL,
  `press_id` int(11) DEFAULT NULL,
  `name` text DEFAULT NULL,
  `email` text DEFAULT NULL,
  `comment` text DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `press_comment`
--

INSERT INTO `press_comment` (`id`, `press_id`, `name`, `email`, `comment`, `created_at`) VALUES
(1, 2, 'Bambang', 'bambang@bambang.com', 'Sip, menginspirasi!', '2022-06-29 10:39:12');

-- --------------------------------------------------------

--
-- Table structure for table `press_like`
--

CREATE TABLE `press_like` (
  `id` int(11) NOT NULL,
  `press_id` int(11) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `press_like`
--

INSERT INTO `press_like` (`id`, `press_id`, `total`, `created_at`) VALUES
(1, 2, 100, '2022-06-29 11:03:16');

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

CREATE TABLE `subscribers` (
  `id` int(11) NOT NULL,
  `name` text DEFAULT NULL,
  `email` text DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `subscribers`
--

INSERT INTO `subscribers` (`id`, `name`, `email`, `created_at`) VALUES
(1, 'Eve', 'eve@gmail.com', '2022-06-22 02:51:20'),
(4, 'Shouldbe Okayest', 'shouldbe@okayest.com', '2022-06-22 02:51:20'),
(5, 'jopanji', 'jopanji@gmail.com', '2022-06-22 02:51:20');

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE `team` (
  `id` int(11) NOT NULL,
  `name` text DEFAULT NULL,
  `position` text DEFAULT NULL,
  `short_description` text DEFAULT NULL,
  `biography` text DEFAULT NULL,
  `photo` text DEFAULT NULL,
  `slug` text DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `team`
--

INSERT INTO `team` (`id`, `name`, `position`, `short_description`, `biography`, `photo`, `slug`, `created_at`) VALUES
(3, 'Chad', 'Chad Chairman', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '1655798957_458a0dbd5d6971914b0a.png', NULL, '2022-06-20 17:00:00'),
(4, 'Karl Marx', 'C.E.O of Communism', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '<p><img style=\"width: 744px;\" src=\"http://localhost:8080/uploads/berkas/1655893218_2d37be736ce9a5a1c4e1.jpg\"><br></p><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p><br></p>', '1655893151_2807b04afbae07bb03b4.jpg', NULL, '2022-06-22 10:19:11');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` text DEFAULT NULL,
  `password` text DEFAULT NULL,
  `salt` text DEFAULT NULL,
  `role` text DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `salt`, `role`, `created_at`) VALUES
(1, 'superadmin1234', 'd4e22790856eef5e3c3bd43c63a6ab9e62aa890f5f4009.40012782', '62aa890f5f4009.40012782', '1', '2022-06-22 02:51:58');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `artikel`
--
ALTER TABLE `artikel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banner_video`
--
ALTER TABLE `banner_video`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `press`
--
ALTER TABLE `press`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `press_comment`
--
ALTER TABLE `press_comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `press_like`
--
ALTER TABLE `press_like`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscribers`
--
ALTER TABLE `subscribers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `artikel`
--
ALTER TABLE `artikel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `banner_video`
--
ALTER TABLE `banner_video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `press`
--
ALTER TABLE `press`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `press_comment`
--
ALTER TABLE `press_comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `press_like`
--
ALTER TABLE `press_like`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `subscribers`
--
ALTER TABLE `subscribers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `team`
--
ALTER TABLE `team`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
