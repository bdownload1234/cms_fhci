<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (is_file(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(false);
// The Auto Routing (Legacy) is very dangerous. It is easy to create vulnerable apps
// where controller filters or CSRF protection are bypassed.
// If you don't want to define all routes, please use the Auto Routing (Improved).
// Set `$autoRoutesImproved` to true in `app/Config/Feature.php` and set the following to true.
//$routes->setAutoRoute(false);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.

$routes->get('/', 'Home::index');

/*
 * --------------------------------------------------------------------
 * Route for API
 * --------------------------------------------------------------------
 */

$routes->get('/api/blog', 'API\Blog::index');
$routes->get('/api/press', 'API\Press::index');
$routes->get('/api/banner', 'API\Banner::index');
$routes->get('/api/bannervideo', 'API\BannerVideo::index');
$routes->get('/api/gallery', 'API\Gallery::index');
$routes->get('/api/event', 'API\Event::index');
$routes->get('/api/team', 'API\Team::index');
// $routes->get('/api/contact', 'API\Contact::index');
// $routes->get('/api/subscriber', 'API\Subscriber::index');

// $routes->get('/api/blog/(:num)', 'API\Blog::show/$1');
$routes->get('/api/blog/(:any)', 'API\Blog::showbyslug/$1');
$routes->get('/api/banner/(:num)', 'API\Banner::show/$1');
$routes->get('/api/gallery/(:num)', 'API\Gallery::show/$1');
$routes->get('/api/event/(:num)', 'API\Event::show/$1');
$routes->get('/api/team/(:any)', 'API\Team::showbyslug/$1');
$routes->get('/api/press/(:any)', 'API\Press::showbyslug/$1');
// $routes->get('/api/contact/(:num)', 'API\Contact::show/$1');
// $routes->get('/api/subscriber/(:num)', 'API\Subscriber::show/$1');

// $routes->post('api/blog', 'API\Blog::create');
// $routes->put('api/blog/(:num)', 'API\Blog::update/$1');
// $routes->delete('api/blog/(:num)', 'API\Blog::delete/$1');

// $routes->post('api/banner', 'API\Banner::create');
// $routes->put('api/banner/(:num)', 'API\Banner::update/$1');
// $routes->delete('api/banner/(:num)', 'API\Banner::delete/$1');

$routes->post('api/contact', 'API\Contact::create');
// $routes->put('api/contact/(:num)', 'API\Contact::update/$1');
// $routes->delete('api/contact/(:num)', 'API\Contact::delete/$1');

// $routes->post('api/event', 'API\Event::create');
// $routes->put('api/event/(:num)', 'API\Event::update/$1');
// $routes->delete('api/event/(:num)', 'API\Event::delete/$1');

$routes->post('api/subscriber', 'API\Subscriber::create');
// $routes->put('api/subscriber/(:num)', 'API\Subscriber::update/$1');
// $routes->delete('api/subscriber/(:num)', 'API\Subscriber::delete/$1');

// $routes->post('api/team', 'API\Team::create');
// $routes->put('api/team/(:num)', 'API\Team::update/$1');
// $routes->delete('api/team/(:num)', 'API\Team::delete/$1');


// $routes->resource('/api/blog',['controller' => 'Api\Blog']);
// $routes->resource('/api/banner',['controller' => 'Api\Banner']);
// $routes->resource('/api/contact',['controller' => 'Api\Contact']);
// $routes->resource('/api/event',['controller' => 'Api\Event']);
// $routes->resource('/api/subscriber',['controller' => 'Api\Subscriber']);
// $routes->resource('/api/team',['controller' => 'Api\Team']);


/*
 * --------------------------------------------------------------------
 * Route for Page
 * --------------------------------------------------------------------
 */

$routes->get('/admin', 'Admin::index');
$routes->get('/admin/banner', 'Admin::banner');
$routes->get('/admin/gallery', 'Admin::gallery');
$routes->get('/admin/bannervideo', 'Admin::bannervideo');
$routes->get('/admin/blog', 'Admin::blog');
$routes->get('/admin/press', 'Admin::press');
$routes->get('/admin/team', 'Admin::team');
$routes->get('/admin/event', 'Admin::event');

$routes->get('/auth/logout', 'Auth::logout');
$routes->get('/auth/login', 'Auth::login');
$routes->get('/auth/register', 'Auth::register');
$routes->post('/auth/valid_login', 'Auth::valid_login');
$routes->post('/auth/valid_register', 'Auth::valid_register');

$routes->post('/banner/simpan', 'Banner::simpan');
$routes->post('/banner/edit', 'Banner::edit');
$routes->get('/banner/hapus/(:num)', 'Banner::hapus/$1');

$routes->post('/gallery/simpan', 'Gallery::simpan');
$routes->post('/gallery/edit', 'Gallery::edit');
$routes->get('/gallery/hapus/(:num)', 'Gallery::hapus/$1');

$routes->post('/bannervideo/simpan', 'BannerVideo::simpan');
$routes->post('/bannervideo/edit', 'BannerVideo::edit');
$routes->get('/bannervideo/hapus/(:num)', 'BannerVideo::hapus/$1');

$routes->get('/blog/view/(:num)', 'Blog::view/$1');
$routes->post('/blog/simpan', 'Blog::simpan');
$routes->post('/blog/edit', 'Blog::edit');
$routes->get('/blog/hapus/(:num)', 'Blog::hapus/$1');

$routes->get('/press/view/(:num)', 'Press::view/$1');
$routes->post('/press/simpan', 'Press::simpan');
$routes->post('/press/edit', 'Press::edit');
$routes->get('/press/hapus/(:num)', 'Press::hapus/$1');

$routes->get('/team/view/(:num)', 'Team::view/$1');
$routes->post('/team/simpan', 'Team::simpan');
$routes->post('/team/edit', 'Team::edit');
$routes->get('/team/hapus/(:num)', 'Team::hapus/$1');

$routes->post('/event/simpan', 'Event::simpan');
$routes->post('/event/edit', 'Event::edit');
$routes->get('/event/hapus/(:num)', 'Event::hapus/$1');

$routes->post('/summernote/upload_image', 'Summernote::upload_image');
$routes->post('/summernote/delete_image', 'Summernote::delete_image');

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (is_file(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
