<?php namespace App\Controllers;
 
use CodeIgniter\Controller;
use App\Models\TeamModel;
 
class Team extends BaseController
{
    public function index()
    {
        $model = new TeamModel();
        if (!$this->validate([]))
        {
            $data['validation'] = $this->validator;
            $data['artikel'] = $model->getTeam();
            return view('admin/index_team',$data);
        }
    }

    public function form(){
        helper('form');
        return view('view_form');
    }

    public function view($id){
        $model = new TeamModel();
        $data['team'] = $model->PilihTeam($id)->getRow();
        return view('admin/show_team',$data);
    }

    public function simpan(){

        $model = new TeamModel();
        if ($this->request->getMethod() !== 'post') {
            return redirect()->to('team');
        }
        $validation = $this->validate([
            'file_upload' => 'uploaded[file_upload]|mime_in[file_upload,image/jpg,image/jpeg,image/gif,image/png]|max_size[file_upload,4096]'
        ]);
 
        if ($validation == FALSE) {
        $data = array(
            'name'  => $this->request->getPost('name'),
            'position' => $this->request->getPost('position'),
            'short_description' => $this->request->getPost('short_description'),
            'biography' => $this->request->getPost('biography'),
            'slug' => url_title($this->request->getPost('name'), '-', true)
        );
        } else {
            
            $upload = $this->request->getFile('file_upload');
            $newName = $upload->getRandomName();
            $upload->move(WRITEPATH . '../../assets/images/',$newName);
            $data = array(
                'name'  => $this->request->getPost('name'),
                'position' => $this->request->getPost('position'),
                'short_description' => $this->request->getPost('short_description'),
                'biography' => $this->request->getPost('biography'),
                'slug' => url_title($this->request->getPost('name'), '-', true),
                'photo' => $newName
            );
        }
        $model->SimpanTeam($data);
        return redirect()->to('admin/team')->with('berhasil', 'Data Berhasil di Simpan');
    }

    public function form_edit($id){
        $model = new TeamModel();
        helper('form');
        $data['artikel'] = $model->PilihTeam($id)->getRow();
        return view('form_edit',$data);
    }

    public function edit(){
        $model = new TeamModel();
        if ($this->request->getMethod() !== 'post') {
            return redirect()->to('/admin/team');
        }
        $id = $this->request->getPost('id');
        $validation = $this->validate([
            'file_upload_edit' => 'uploaded[file_upload_edit]|mime_in[file_upload_edit,image/jpg,image/jpeg,image/gif,image/png]|max_size[file_upload_edit,4096]'
        ]);
 
        if ($validation == FALSE) {
        $data = array(
            'name'  => $this->request->getPost('name'),
            'position' => $this->request->getPost('position'),
            'short_description' => $this->request->getPost('short_description'),
            'biography' => $this->request->getPost('biography'),
            'slug' => url_title($this->request->getPost('title'), '-', true)
        );
        } else {
        $dt = $model->PilihTeam($id)->getRow();
        $gambar = $dt->photo;
        $path = '../../assets/images/';
        @unlink($path.$gambar);
            $upload = $this->request->getFile('file_upload_edit');
            $upload->move(WRITEPATH . '../../assets/images/');
        $data = array(
            'name'  => $this->request->getPost('name'),
            'position' => $this->request->getPost('position'),
            'short_description' => $this->request->getPost('short_description'),
            'biography' => $this->request->getPost('biography'),
            'slug' => url_title($this->request->getPost('title'), '-', true),
            'photo' => $upload->getName()
        );
        }
        $model->EditTeam($id,$data);
        return redirect()->to('/admin/team')->with('berhasil', 'Data Berhasil di Ubah');
        
    }

    public function hapus($id){
        $model = new TeamModel();
        $dt = $model->PilihTeam($id)->getRow();
        $model->HapusTeam($id);
        $gambar = $dt->photo;
        $path = '../../assets/images/';
        @unlink($path.$gambar);
        return redirect()->to('/admin/team')->with('berhasil', 'Data Berhasil di Hapus');
    }

}