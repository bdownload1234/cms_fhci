<?php namespace App\Controllers;
 
use CodeIgniter\Controller;
use App\Models\BannerVideoModel;
 
class BannerVideo extends BaseController
{
    public function index()
    {
        $model = new BannerVideoModel();
        if (!$this->validate([]))
        {
            $data['validation'] = $this->validator;
            $data['bannervideo'] = $model->getBannerVideo();
            return view('admin/index_bannervideo',$data);
        }
    }

    public function form(){
        helper('form');
        return view('view_form');
    }

    public function view($id){
        $model = new BannerVideoModel();
        $data['bannervideo'] = $model->PilihBannerVideo($id)->getRow();
        return view('view',$data);
    }

    public function simpan(){

        $model = new BannerVideoModel();
        if ($this->request->getMethod() !== 'post') {
            return redirect()->to('admin/bannervideo');
        }
        $validation = $this->validate([
            'file_upload' => 'uploaded[file_upload]|mime_in[file_upload,video/mp4,video/x-m4v,video/*]|max_size[file_upload,4096]'
        ]);
 
        if ($validation == FALSE) {
        $data = array(
            'title'  => $this->request->getPost('title'),
            'slug' => url_title($this->request->getPost('title'), '-', true)
        );
        } else {
            $upload = $this->request->getFile('file_upload');
            $newName = $upload->getRandomName();
            $upload->move(WRITEPATH . '../../assets/videos/',$newName);
            $data = array(
                'title'  => $this->request->getPost('title'),
                'slug' => url_title($this->request->getPost('title'), '-', true),
                'video' => $newName
            );
        }
        $model->SimpanBannerVideo($data);
        // var_dump($this->request->getFile('file_upload'));
        return redirect()->to('admin/bannervideo')->with('berhasil', 'Data Berhasil di Simpan');
    }

    public function form_edit($id){
        $model = new BannerVideoModel();
        helper('form');
        $data['artikel'] = $model->PilihBannerVideo($id)->getRow();
        return view('form_edit',$data);
    }

    public function edit(){
        $model = new BannerVideoModel();
        if ($this->request->getMethod() !== 'post') {
            return redirect()->to('bannervideo');
        }
        $id = $this->request->getPost('id');
        $validation = $this->validate([
            'file_upload_edit' => 'uploaded[file_upload_edit]|mime_in[file_upload_edit,video/mp4,video/x-m4v,video/*]|max_size[file_upload_edit,4096]'
        ]);
 
        if ($validation == FALSE) {
            $data = array(
            'title'  => $this->request->getPost('title'),
            'slug' => url_title($this->request->getPost('title'), '-', true)
        );
        } else {
            $dt = $model->PilihBannerVideo($id)->getRow();
            $gambar = $dt->video;
            $path = '../../assets/videos/';
            @unlink($path.$gambar);
            $upload = $this->request->getFile('file_upload_edit');
            // $newName = $upload->getRandomName();
            $upload->move(WRITEPATH . '../../assets/videos/');
            $data = array(
                'title'  => $this->request->getPost('title'),
                'slug' => url_title($this->request->getPost('title'), '-', true),
                'video' => $upload->getName()
            );
        }
        $model->EditBannerVideo($id,$data);
        // var_dump(empty($this->request->getFile('file_upload_edit')));
        return redirect()->to('admin/bannervideo')->with('berhasil', 'Data Berhasil di Ubah');
    }

    public function hapus($id){
        // $id = $this->request->getPost('id');
        $model = new BannerVideoModel();
        $dt = $model->PilihBannerVideo($id)->getRow();
        $model->HapusBannerVideo($id);
        $gambar = $dt->video;
        $path = '../../assets/videos/';
        @unlink($path.$gambar);
        return redirect()->to('admin/bannervideo')->with('berhasil', 'Data Berhasil di Hapus');
    }

}