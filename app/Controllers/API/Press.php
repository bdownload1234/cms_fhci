<?php

namespace App\Controllers\api;

use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;
use App\Models\PressModel;
use CodeIgniter\HTTP\Response;

class Press extends ResourceController
{
    use ResponseTrait;
    // all users
    public function index()
    {
        $model = new PressModel();
        $data['press'] = $model->orderBy('id', 'DESC')->findAll();
        $db      = \Config\Database::connect();
        $comment = $db->query("SELECT press_id, COUNT(*) as total from press_comment");
        $like = $db->query("SELECT press_id, total from press_like");
        return $this->respond([
            'press' => $data['press'],
            'comment' => $comment->getResult(),
            'like' => $like->getResult(),
        ]);
    }

    public function like($id){
        $db      = \Config\Database::connect();
        $like = $db->query("UPDATE press_like SET total = total + 1 WHERE press_id = $id");
        return $this->respond([
            'status' => 200,
            'message' => 'oke'
        ]);
    }

    // create
    public function create()
    {
        $model = new PressModel();
        $data = [
            'title' => $this->request->getVar('title'),
            'content'  => $this->request->getVar('content'),
        ];
        $model->insert($data);
        $response = [
            'status'   => 201,
            'error'    => null,
            'messages' => [
                'success' => 'Data berhasil ditambahkan.'
            ]
        ];
        return $this->respondCreated($response);
    }
    // single user
    public function show($id = null)
    {
        $model = new PressModel();
        $data = $model->where('id', $id)->first();
        if ($data) {
            return $this->respond($data);
        } else {
            return $this->failNotFound('Data tidak ditemukan.');
        }
    }

    public function showbyslug($slug = null)
    {
        $model = new PressModel();
        $data = $model->where('slug', $slug)->first();
        $db      = \Config\Database::connect();
        $comment = $db->query("SELECT name, email, comment, created_at as date FROM press_comment WHERE press_id = $data[id]");
        $like = $db->query("SELECT total FROM press_like WHERE press_id = $data[id]");
        if ($data) {
            return $this->respond([
                'press' => $data,
                'comment' => $comment->getResult(),
                'like' => $like->getResult(),
            ]);
        } else {
            return $this->failNotFound('Data tidak ditemukan.');
        }
    }
    // update
    public function update($id = null)
    {
        $model = new PressModel();
        $id = $this->request->getVar('id');
        $data = [
            'title' => $this->request->getVar('title'),
            'content'  => $this->request->getVar('content'),
        ];
        $model->update($id, $data);
        $response = [
            'status'   => 200,
            'error'    => null,
            'messages' => [
                'success' => 'Data berhasil diubah.'
            ]
        ];
        return $this->respond($response);
    }
    // delete
    public function delete($id = null)
    {
        $model = new PressModel();
        $data = $model->where('id', $id)->delete($id);
        if ($data) {
            $model->delete($id);
            $response = [
                'status'   => 200,
                'error'    => null,
                'messages' => [
                    'success' => 'Data berhasil dihapus.'
                ]
            ];
            return $this->respondDeleted($response);
        } else {
            return $this->failNotFound('Data tidak ditemukan.');
        }
    }
}