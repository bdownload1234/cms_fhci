<?php namespace App\Controllers;
 
use CodeIgniter\Controller;
use App\Models\BlogModel;
 
class Blog extends BaseController
{
    public function index()
    {
        $model = new BlogModel();
        if (!$this->validate([]))
        {
            $data['validation'] = $this->validator;
            $data['artikel'] = $model->getArtikel();
            return view('admin/index_blog',$data);
        }
    }

    public function form(){
        helper('form');
        return view('view_form');
    }

    public function view($id){
        $model = new BlogModel();
        $data['artikel'] = $model->PilihBlog($id)->getRow();
        return view('admin/show_blog',$data);
    }

    public function simpan(){

        $model = new BlogModel();
        if ($this->request->getMethod() !== 'post') {
            return redirect()->to('blog');
        }
        $validation = $this->validate([
            'file_upload' => 'uploaded[file_upload]|mime_in[file_upload,image/jpg,image/jpeg,image/gif,image/png]|max_size[file_upload,4096]'
        ]);
 
        if ($validation == FALSE) {
        $data = array(
            'title'  => $this->request->getPost('title'),
            'content' => $this->request->getPost('content'),
            'author' => $this->request->getPost('author'),
            'meta_description' => $this->request->getPost('meta-description'),
            'slug' => url_title($this->request->getPost('title'), '-', true)
        );
        } else {
            
            $upload = $this->request->getFile('file_upload');
            $newName = $upload->getRandomName();
            $upload->move(WRITEPATH . '../../assets/images/',$newName);
            $data = array(
                'title'  => $this->request->getPost('title'),
                'content' => $this->request->getPost('content'),
                'author' => $this->request->getPost('author'),
                'meta_description' => $this->request->getPost('meta-description'),
                'slug' => url_title($this->request->getPost('title'), '-', true),
                'thumbnail' => $newName
            );
        }
        $model->SimpanBlog($data);
        return redirect()->to('admin/blog')->with('berhasil', 'Data Berhasil di Simpan');
    }

    public function form_edit($id){
        $model = new BlogModel();
        helper('form');
        $data['artikel'] = $model->PilihBlog($id)->getRow();
        return view('form_edit',$data);
    }

    public function edit(){
        $model = new BlogModel();
        if ($this->request->getMethod() !== 'post') {
            return redirect()->to('blog');
        }
        $id = $this->request->getPost('id');
        $validation = $this->validate([
            'file_upload_edit' => 'uploaded[file_upload_edit]|mime_in[file_upload_edit,image/jpg,image/jpeg,image/gif,image/png]|max_size[file_upload_edit,4096]'
        ]);
 
        if ($validation == FALSE) {
        $data = array(
            'title'  => $this->request->getPost('title'),
            'content' => $this->request->getPost('content'),
            'meta_description' => $this->request->getPost('meta-description'),
            'slug' => url_title($this->request->getPost('title'), '-', true)
        );
        } else {
        $dt = $model->PilihBlog($id)->getRow();
        $gambar = $dt->thumbnail;
        $path = '../../assets/images/';
        @unlink($path.$gambar);
            $upload = $this->request->getFile('file_upload_edit');
            $upload->move(WRITEPATH . '../../assets/images/');
        $data = array(
            'title'  => $this->request->getPost('title'),
            'content' => $this->request->getPost('content'),
            'meta_description' => $this->request->getPost('meta-description'),
            'slug' => url_title($this->request->getPost('title'), '-', true),
            'thumbnail' => $upload->getName()
        );
        }
        $model->EditBlog($id,$data);
        return redirect()->to('admin/blog')->with('berhasil', 'Data Berhasil di Ubah');
        
    }

    public function hapus($id){
        $model = new BlogModel();
        $dt = $model->PilihBlog($id)->getRow();
        $model->HapusBlog($id);
        $gambar = $dt->thumbnail;
        $path = '../../assets/images/';
        @unlink($path.$gambar);
        return redirect()->to('admin/blog')->with('berhasil', 'Data Berhasil di Hapus');
    }

}