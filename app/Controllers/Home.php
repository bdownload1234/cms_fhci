<?php

namespace App\Controllers;

class Home extends BaseController
{
    public function index()
    {
        // return view('auth/login');
        //cek apakah ada session bernama isLogin
        if(!$this->session->has('isLogin')){
            return redirect()->to('/auth/login');
        }
        
        //cek role dari session
        if($this->session->get('role') != 1){
            return redirect()->to('/user');
        }
        
        return view('admin/index');
    }
}
