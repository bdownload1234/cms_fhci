<?php

namespace App\Controllers;
use App\Models\BlogModel;
use App\Models\PressModel;
use App\Models\BannerModel;
use App\Models\BannerVideoModel;
use App\Models\GalleryModel;
use App\Models\TeamModel;
use App\Models\EventModel;
use App\Models\SubscriberModel;

class Admin extends BaseController
{
    
    public function __construct()
    {
        $this->session = session();
    }
    
    public function index()
    {
        //cek apakah ada session bernama isLogin
        if(!$this->session->has('isLogin')){
            return redirect()->to('/auth/login');
        }
        
        //cek role dari session
        if($this->session->get('role') != 1){
            return redirect()->to('/user');
        }
        
        return view('admin/index');
        
    }
    public function blog()
    {
        //cek apakah ada session bernama isLogin
        if(!$this->session->has('isLogin')){
            return redirect()->to('/auth/login');
        }
        
        //cek role dari session
        if($this->session->get('role') != 1){
            return redirect()->to('/user');
        }
        
        $model = new BlogModel();
        if (!$this->validate([]))
        {
            $data['validation'] = $this->validator;
            $data['artikel'] = $model->orderBy('id', 'DESC')->paginate(8, 'artikel');
            $data['pager'] = $model->pager;
            return view('admin/index_blog',$data);
        }
        
    }

    public function press()
    {
        //cek apakah ada session bernama isLogin
        if(!$this->session->has('isLogin')){
            return redirect()->to('/auth/login');
        }
        
        //cek role dari session
        if($this->session->get('role') != 1){
            return redirect()->to('/user');
        }
        
        $model = new PressModel();
        if (!$this->validate([]))
        {
            $data['validation'] = $this->validator;
            $data['press'] = $model->orderBy('id', 'DESC')->paginate(8, 'press');
            $data['pager'] = $model->pager;
            return view('admin/index_press',$data);
        }
        
    }

    public function gallery()
    {
        //cek apakah ada session bernama isLogin
        if(!$this->session->has('isLogin')){
            return redirect()->to('/auth/login');
        }
        
        //cek role dari session
        if($this->session->get('role') != 1){
            return redirect()->to('/user');
        }
        
        $model = new GalleryModel();
        if (!$this->validate([]))
        {
            $data['validation'] = $this->validator;
            $data['gallery'] = $model->orderBy('id', 'DESC')->paginate(8, 'gallery');
            $data['pager'] = $model->pager;
            return view('admin/index_gallery',$data);
        }
        
    }

    public function banner()
    {
        //cek apakah ada session bernama isLogin
        if(!$this->session->has('isLogin')){
            return redirect()->to('/auth/login');
        }
        
        //cek role dari session
        if($this->session->get('role') != 1){
            return redirect()->to('/user');
        }
        
        $model = new BannerModel();
        if (!$this->validate([]))
        {
            $data['validation'] = $this->validator;
            $data['banner'] = $model->orderBy('id', 'DESC')->paginate(12, 'banner');
            $data['pager'] = $model->pager;
            return view('admin/index_banner',$data);
        }
        
    }

    public function bannervideo()
    {
        //cek apakah ada session bernama isLogin
        if(!$this->session->has('isLogin')){
            return redirect()->to('/auth/login');
        }
        
        //cek role dari session
        if($this->session->get('role') != 1){
            return redirect()->to('/user');
        }
        
        $model = new BannerVideoModel();
        if (!$this->validate([]))
        {
            $data['validation'] = $this->validator;
            $data['bannervideo'] = $model->orderBy('id', 'DESC')->paginate(12, 'bannervideo');
            $data['pager'] = $model->pager;
            return view('admin/index_bannervideo',$data);
        }
        
    }

    public function event()
    {
        //cek apakah ada session bernama isLogin
        if(!$this->session->has('isLogin')){
            return redirect()->to('/auth/login');
        }
        
        //cek role dari session
        if($this->session->get('role') != 1){
            return redirect()->to('/user');
        }
        
        $model = new EventModel();
        if (!$this->validate([]))
        {
            $data['validation'] = $this->validator;
            $data['event'] = $model->paginate(12, 'event');
            $data['pager'] = $model->pager;
            return view('admin/index_event',$data);
        }
        
    }

    public function team()
    {
        //cek apakah ada session bernama isLogin
        if(!$this->session->has('isLogin')){
            return redirect()->to('/auth/login');
        }
        
        //cek role dari session
        if($this->session->get('role') != 1){
            return redirect()->to('/user');
        }
        
        $model = new TeamModel();
        if (!$this->validate([]))
        {
            $data['validation'] = $this->validator;
            $data['team'] = $model->paginate(12, 'team');
            $data['pager'] = $model->pager;
            return view('admin/index_team',$data);
        }
        
    }

    public function subscriber()
    {
        //cek apakah ada session bernama isLogin
        if(!$this->session->has('isLogin')){
            return redirect()->to('/auth/login');
        }
        
        //cek role dari session
        if($this->session->get('role') != 1){
            return redirect()->to('/user');
        }
        
        $model = new SubscriberModel();
        if (!$this->validate([]))
        {
            $data['validation'] = $this->validator;
            $data['subscriber'] = $model->findAll();
            return view('admin/index_subscriber',$data);
        }
        
    }

    public function contact()
    {
        //cek apakah ada session bernama isLogin
        if(!$this->session->has('isLogin')){
            return redirect()->to('/auth/login');
        }
        
        //cek role dari session
        if($this->session->get('role') != 1){
            return redirect()->to('/user');
        }
        
        $model = new BannerModel();
        if (!$this->validate([]))
        {
            $data['validation'] = $this->validator;
            $data['banner'] = $model->paginate(12, 'banner');
            $data['pager'] = $model->pager;
            return view('admin/index_banner',$data);
        }
        
    }

    
    
}