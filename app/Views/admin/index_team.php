<?= $this->extend('layout/page_layout') ?>

<?= $this->section('content') ?>
<div class="container-fluid">
    <div class="layout-specing">
        <div class="d-md-flex justify-content-between">
            <div>
                <h5 class="mb-0">Team</h5>

                <nav aria-label="breadcrumb" class="d-inline-block mt-1">
                    <ul class="breadcrumb breadcrumb-muted bg-transparent rounded mb-0 p-0">
                        <li class="breadcrumb-item text-capitalize"><a href=<?= base_url("/admin/") ?>>Home</a></li>
                        <li class="breadcrumb-item text-capitalize active" aria-current="page">Team</li>
                    </ul>
                </nav>
            </div>

            <div class="mt-4 mt-sm-0">
                <a href="#" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#newblogadd">Add Team</a>
            </div>
        </div>

        <div class="row">

            <?php foreach ($team as $row) : ?>
                <div class="col-lg-3 col-md-6 mt-4 pt-2">
                    <div class="card team team-primary text-center border-0 rounded">
                        <div class="position-relative pt-2">
                        <a href=<?= base_url("/team/view/".$row['id'])?>>
                            <?php
                            if (!empty($row["photo"])) {
                                echo '<img src="' . base_url("assets/images/$row[photo]") . '" class="img-fluid avatar avatar-ex-large rounded-circle shadow" alt="...">';
                            } else {
                                echo '<img src="' . base_url("assets/images/blog/no-image.jpg") . '" class="img-fluid avatar avatar-ex-large rounded-circle shadow" alt="...">';
                            }
                            ?>
                        </a>
                            <!-- <ul class="list-unstyled mb-0 team-icon">
                                <li class="list-inline-item"><a href="javascript:void(0)" class="btn btn-primary btn-pills btn-sm btn-icon"><i data-feather="facebook" class="icons"></i></a></li>
                                <li class="list-inline-item"><a href="javascript:void(0)" class="btn btn-primary btn-pills btn-sm btn-icon"><i data-feather="instagram" class="icons"></i></a></li>
                                <li class="list-inline-item"><a href="javascript:void(0)" class="btn btn-primary btn-pills btn-sm btn-icon"><i data-feather="twitter" class="icons"></i></a></li>
                                <li class="list-inline-item"><a href="javascript:void(0)" class="btn btn-primary btn-pills btn-sm btn-icon"><i data-feather="linkedin" class="icons"></i></a></li>
                            </ul> -->
                            <!--end icon-->
                        </div>
                        <div class="card-body py-3 px-0 content">
                        <h5 class="mb-0"><a href=<?= base_url("/team/view/".$row['id'])?> class="name text-dark"><?= $row['name']; ?></a></h5>
                            <!-- <h5 class="mb-0"><a href="javascript:void(0)" class="name text-dark"><?= $row['name']; ?></a></h5> -->
                            <small class="designation text-muted"><?= $row['position']; ?></small>
                        </div>
                    </div>
                </div>
                <!--end col-->
            
            <?php endforeach; ?>
        </div>
        <!--end row-->



        <div class="row">
            <!-- PAGINATION START -->
            <?= $pager->links('team', 'pagination') ?>
            <!-- PAGINATION END -->
        </div>
        <!--end row-->
    </div>
</div>
<!--end container-->

<!-- Start Modal -->
<div class="modal fade" id="newblogadd" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header border-bottom p-3">
                <h5 class="modal-title" id="exampleModalLabel">Add Team</h5>
                <button type="button" class="btn btn-icon btn-close" data-bs-dismiss="modal" id="close-modal"><i class="uil uil-times fs-4 text-dark"></i></button>
            </div>

            <div class="modal-body p-3 pt-4">
                <div class="row">
                    <form method="post" action=<?=base_url("/team/simpan")?> enctype="multipart/form-data">
                        <div class="col-md-12 mt-4 mt-sm-0">
                            <div>
                                <!-- <form method="post" action="/blog/simpan"> -->
                                <div class="row">

                                    <div class="col-12">
                                        <div class="mb-3">
                                            <label class="form-label">Name <span class="text-danger">*</span></label>
                                            <input name="name" id="name" type="text" class="form-control" placeholder="Name :">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="mb-3">
                                            <label class="form-label">Position <span class="text-danger">*</span></label>
                                            <input name="position" id="position" type="text" class="form-control" placeholder="Position :">
                                        </div>
                                    </div>
                                    <!--end col-->

                                    <div class="col-lg-12">
                                        <div class="mb-3">
                                            <label class="form-label">Short Description <span class="text-danger">*</span></label>
                                            <textarea name="short_description" rows="4" class="form-control" placeholder="Short Description :"></textarea>
                                        </div>
                                    </div>
                                    <!--end col-->

                                    <div class="col-lg-12">
                                        <div class="mb-3">
                                            <label class="form-label">Biography <span class="text-danger">*</span></label>
                                            <textarea name="biography" class="summernote form-control" id="summernote" rows="4" placeholder="Biography :"></textarea>
                                        </div>
                                    </div>
                                    <!--end col-->

                                    <div class="col-12">
                                        <div class="mb-3">
                                            <p class="text-muted">Upload your team photo here, Please click "Upload Image" Button.</p>
                                            <div class="preview-box d-block justify-content-center rounded shadow overflow-hidden bg-light p-1"></div>
                                            <input type="file" id="input-file" name="file_upload" accept="image/*" onchange={handleChange()} hidden />
                                            <label class="btn-upload btn btn-primary mt-4" for="input-file">Upload Image</label>
                                        </div>
                                    </div>
                                    <!--end col-->

                                    <div class="col-lg-12 text-end">
                                        <button type="submit" class="btn btn-primary">Add Team</button>
                                    </div>
                                    <!--end col-->
                                </div>
                                <!-- </form> -->
                            </div>
                        </div>
                        <!--end col-->
                    </form>
                </div>
                <!--end row-->
            </div>
        </div>
    </div>
</div>
<!-- End modal -->


<style>
    .note-editable {
        background-color: #FFFFFF !important;
    }

    .card-img-top {
        width: 100%;
        height: 15vw;
        object-fit: cover;
    }
</style>

<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>
<script>
    $(document).ready(function() {
        $('.summernote').summernote({
            callbacks: {
                onImageUpload: function(files) {
                    for (let i = 0; i < files.length; i++) {
                        $.upload(files[i]);
                    }
                },
                onMediaDelete: function(target) {
                    $.delete(target[0].src);
                }
            },
            height: 200,
            toolbar: [
                ["style", ["bold", "italic", "underline", "clear"]],
                ["fontname", ["fontname"]],
                ["fontsize", ["fontsize"]],
                ["color", ["color"]],
                ["para", ["ul", "ol", "paragraph"]],
                ["height", ["height"]],
                ["insert", ["link", "picture", "imageList", "video", "fullscreen"]],

            ],
            imageList: {
                endpoint: "<?php echo site_url('berita/listGambar') ?>",
                fullUrlPrefix: "<?php echo base_url('uploads/berkas') ?>/",
                thumbUrlPrefix: "<?php echo base_url('uploads/berkas') ?>/"
            }
        });

        $.upload = function(file) {
            let out = new FormData();
            out.append('file', file, file.name);
            $.ajax({
                method: 'POST',
                url: '<?php echo site_url('summernote/upload_image') ?>',
                contentType: false,
                cache: false,
                processData: false,
                data: out,
                success: function(img) {
                    $('.summernote').summernote('insertImage', img);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.error(textStatus + " " + errorThrown);
                }
            });
        };
        $.delete = function(src) {
            $.ajax({
                method: 'POST',
                url: '<?php echo site_url('summernote/delete_image') ?>',
                cache: false,
                data: {
                    src: src
                },
                success: function(response) {
                    console.log(response);
                }

            });
        };
    });

    function konfirmasi(url) {
        var result = confirm("Want to delete?");
        if (result) {
            window.location.href = url;
        }
    }
</script>
<script>
    const handleChange = () => {
        const fileUploader = document.querySelector('#input-file');
        const getFile = fileUploader.files
        if (getFile.length !== 0) {
            const uploadedFile = getFile[0];
            readFile(uploadedFile);
        }
    }

    const readFile = (uploadedFile) => {
        if (uploadedFile) {
            const reader = new FileReader();
            reader.onload = () => {
                const parent = document.querySelector('.preview-box');
                parent.innerHTML = `<img class="preview-content img-fluid" src=${reader.result} />`;
            };
            reader.readAsDataURL(uploadedFile);
        }
    }
</script>

<?= $this->endSection() ?>