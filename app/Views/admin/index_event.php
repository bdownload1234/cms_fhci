<?= $this->extend('layout/page_layout') ?>

<?= $this->section('content') ?>
                <div class="container-fluid">
                    <div class="layout-specing">
                        <div class="d-md-flex justify-content-between">
                            <div>
                                <h5 class="mb-0">Event</h5>

                                <nav aria-label="breadcrumb" class="d-inline-block mt-1">
                                    <ul class="breadcrumb breadcrumb-muted bg-transparent rounded mb-0 p-0">
                                        <li class="breadcrumb-item text-capitalize"><a href=<?= base_url("/admin/")?>>Home</a></li>
                                        <li class="breadcrumb-item text-capitalize active" aria-current="page">Event</li>
                                    </ul>
                                </nav>
                            </div>

                            <div class="mt-4 mt-sm-0">
                                <a href="#" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#newblogadd">Add Event</a>
                            </div>
                        </div>
                    
                        <div class="row">
                            <?php foreach($event as $row):?>

                            <div class="col-xl-3 col-lg-4 col-md-6 mt-4">
                                <div class="card blog blog-primary rounded border-0 shadow overflow-hidden">
                                    <div class="card-body content">
                                        <h5><a href="javascript:void(0)" class="card-title title text-dark"><?=$row['title'];?> at <?=date("l, j F Y", strtotime($row['date']));?></a></h5>
                                        <div class="post-meta d-flex justify-content-between mt-3">
                                            <?php
                                                echo '<a href="#" class="text-muted readmore" data-bs-toggle="modal" data-bs-target="#editblog" data-title="'.$row['title'].'" data-short_description="'.$row['short_description'].'" data-date="'.$row['date'].'" data-time_from="'.$row['time_from'].'" data-time_to="'.$row['time_to'].'" data-id="'.$row['id'].'">Edit <i class="uil uil-angle-right-b align-middle"></i></a>';
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div><!--end col-->
                            <?php endforeach;?>
                        </div><!--end row-->
                        <div class="row">
                            <?= $pager->links('team', 'pagination') ?>          
                        </div><!--end row-->
                    </div>
                </div><!--end container-->

                <!-- Start Modal -->
                <div class="modal fade" id="newblogadd" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header border-bottom p-3">
                                <h5 class="modal-title" id="exampleModalLabel">Add Event</h5>
                                <button type="button" class="btn btn-icon btn-close" data-bs-dismiss="modal" id="close-modal"><i class="uil uil-times fs-4 text-dark"></i></button>
                            </div>

                            <div class="modal-body p-3 pt-4">
                                <div class="row">
                                    <!-- <div class="col-md-4">
                                        <div class="d-grid">
                                            <p class="text-muted">Upload your blog thumbnail here, Please click "Upload Image" Button.</p>
                                            <div class="preview-box d-block justify-content-center rounded shadow overflow-hidden bg-light p-1"></div>
                                            <input type="file" id="input-file" name="input-file" accept="image/*" onchange={handleChange()} hidden />
                                            <label class="btn-upload btn btn-primary mt-4" for="input-file">Upload Image</label>
                                        </div>
                                    </div> -->
                                    <form method="post" action=<?=base_url("/event/simpan")?> enctype="multipart/form-data">
                                    <div class="col-md-12 mt-4 mt-sm-0">
                                        <div>
                                            <!-- <form method="post" action="/blog/simpan"> -->
                                                <div class="row">

                                                    <div class="col-12">
                                                        <div class="mb-3">
                                                            <label class="form-label">Title <span class="text-danger">*</span></label>
                                                            <input name="title" id="eventitle" type="text" class="form-control" placeholder="Title of Event :">
                                                        </div>
                                                    </div><!--end col-->

                                                    <div class="col-12">
                                                        <div class="mb-3">
                                                            <label class="form-label">Date <span class="text-danger">*</span></label>
                                                            <input name="date" id="date" type="text" class="form-control" placeholder="Date of Event :">
                                                        </div>
                                                    </div><!--end col-->

                                                    <div class="col-12">
                                                        <div class="mb-3">
                                                            <label class="form-label">Time From <span class="text-danger">*</span></label>
                                                            <input name="time_from" id="time-from" type="text" class="form-control" placeholder="Time of Event : e.g 09:30">
                                                        </div>
                                                    </div><!--end col-->

                                                    <div class="col-12">
                                                        <div class="mb-3">
                                                            <label class="form-label">Time To <span class="text-danger">*</span></label>
                                                            <input name="time_to" id="time-to" type="text" class="form-control" placeholder="Endtime of Event : e.g 12:00 or End">
                                                        </div>
                                                    </div><!--end col-->
            
                                                    <!-- <div class="col-md-6">
                                                        <div class="mb-3">
                                                            <label class="form-label"> Date : </label>
                                                            <input name="date" type="text" class="form-control" id="date" value="09 January 2021">
                                                        </div>
                                                    </div>
            
                                                    <div class="col-md-6">
                                                        <div class="mb-3">
                                                            <label class="form-label"> Time to read : </label>
                                                            <input name="time" type="text" class="form-control" id="time" value="5 min read">
                                                        </div>
                                                    </div>
            
                                                    <div class="col-md-6">
                                                        <div class="mb-3">
                                                            <label class="form-label">Tag</label>
                                                            <select class="form-control">
                                                                <option value="BU">Business</option>
                                                                <option value="AG">Agency</option>
                                                                <option value="FA">Fashion</option>
                                                                <option value="SE">Selling</option>
                                                                <option value="FI">Finance</option>
                                                            </select>
                                                        </div>
                                                    </div> -->

                                                    <div class="col-lg-12">
                                                        <div class="mb-3">
                                                            <label class="form-label">Short Description <span class="text-danger">*</span></label>
                                                            <textarea name="short_description" rows="4" class="form-control" placeholder="Short Description :"></textarea>
                                                        </div>
                                                    </div><!--end col-->

                                                    <!-- <div class="col-12">
                                                        <div class="mb-3">
                                                            <p class="text-muted">Upload your team photo here, Please click "Upload Image" Button.</p>
                                                            <div class="preview-box d-block justify-content-center rounded shadow overflow-hidden bg-light p-1"></div>
                                                            <input type="file" id="input-file" name="file_upload" accept="image/*" onchange={handleChange()} hidden />
                                                            <label class="btn-upload btn btn-primary mt-4" for="input-file">Upload Image</label>
                                                        </div>
                                                    </div> -->
            
                                                    <div class="col-lg-12 text-end">
                                                        <button type="submit" class="btn btn-primary">Add Event</button>
                                                    </div><!--end col-->
                                                </div>
                                            <!-- </form> -->
                                        </div>
                                    </div><!--end col-->
                                    </form>
                                </div><!--end row-->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End modal -->

                <!-- Start Modal Edit -->
                <div class="modal fade" id="editblog" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header border-bottom p-3">
                                <h5 class="modal-title" id="exampleModalLabel">Add Event</h5>
                                <button type="button" class="btn btn-icon btn-close" data-bs-dismiss="modal" id="close-modal"><i class="uil uil-times fs-4 text-dark"></i></button>
                            </div>

                            <div class="modal-body p-3 pt-4">
                                <div class="row">
                                    <!-- <div class="col-md-4">
                                        <div class="d-grid">
                                            <p class="text-muted">Upload your blog thumbnail here, Please click "Upload Image" Button.</p>
                                            <div class="preview-box d-block justify-content-center rounded shadow overflow-hidden bg-light p-1"></div>
                                            <input type="file" id="input-file" name="input-file" accept="image/*" onchange={handleChange()} hidden />
                                            <label class="btn-upload btn btn-primary mt-4" for="input-file">Upload Image</label>
                                        </div>
                                    </div> -->
                                    <form method="post" action=<?=base_url("/event/edit")?> enctype="multipart/form-data">
                                    <div class="col-md-12 mt-4 mt-sm-0">
                                        <div>
                                            <!-- <form method="post" action="/blog/simpan"> -->
                                                <div class="row">

                                                    <div class="col-12">
                                                        <div class="mb-3">
                                                            <label class="form-label">Title <span class="text-danger">*</span></label>
                                                            <input name="title" id="blogtitle" type="text" class="form-control" placeholder="Title of Event :">
                                                        </div>
                                                    </div><!--end col-->

                                                    <div class="col-12">
                                                        <div class="mb-3">
                                                            <label class="form-label">Date <span class="text-danger">*</span></label>
                                                            <input name="date" id="date-edit" type="text" class="form-control" placeholder="Date of Event :" disabled>
                                                        </div>
                                                    </div><!--end col-->

                                                    <div class="col-12">
                                                        <div class="mb-3">
                                                            <label class="form-label">Time From <span class="text-danger">*</span></label>
                                                            <input name="time_from" id="time-from" type="text" class="form-control" placeholder="Time of Event : e.g 09:30">
                                                        </div>
                                                    </div><!--end col-->

                                                    <div class="col-12">
                                                        <div class="mb-3">
                                                            <label class="form-label">Time To <span class="text-danger">*</span></label>
                                                            <input name="time_to" id="time-to" type="text" class="form-control" placeholder="Endtime of Event : e.g 12:00 or End">
                                                        </div>
                                                    </div><!--end col-->
                                
            
                                                    <!-- <div class="col-md-6">
                                                        <div class="mb-3">
                                                            <label class="form-label"> Date : </label>
                                                            <input name="date" type="text" class="form-control" id="date" value="09 January 2021">
                                                        </div>
                                                    </div>
            
                                                    <div class="col-md-6">
                                                        <div class="mb-3">
                                                            <label class="form-label"> Time to read : </label>
                                                            <input name="time" type="text" class="form-control" id="time" value="5 min read">
                                                        </div>
                                                    </div>
            
                                                    <div class="col-md-6">
                                                        <div class="mb-3">
                                                            <label class="form-label">Tag</label>
                                                            <select class="form-control">
                                                                <option value="BU">Business</option>
                                                                <option value="AG">Agency</option>
                                                                <option value="FA">Fashion</option>
                                                                <option value="SE">Selling</option>
                                                                <option value="FI">Finance</option>
                                                            </select>
                                                        </div>
                                                    </div> -->
            
                                                    <div class="col-lg-12">
                                                        <div class="mb-3">
                                                            <label class="form-label">Short Description <span class="text-danger">*</span></label>
                                                            <textarea name="short_description" id="short-description" rows="4" class="form-control" placeholder="Short Description :"></textarea>
                                                        </div>
                                                    </div><!--end col-->
            
                                                    <!-- <div class="col-lg-12">
                                                        <div class="mb-3">
                                                            <label class="form-label">Biography <span class="text-danger">*</span></label>
                                                            <textarea name="biography" id="summernote-edit" rows="4" placeholder="Biography :"></textarea>
                                                        </div>
                                                    </div> -->

                                                    <!-- <div class="col-12">
                                                        <div class="mb-3">
                                                            <p class="text-muted">Upload your team photo here, Please click "Upload Image" Button.</p>
                                                            <div class="preview-box2 d-block justify-content-center rounded shadow overflow-hidden bg-light p-1">
                                                                <img class="preview-content img-fluid" src="" />
                                                            </div>
                                                            <input type="file" id="input-file-edit" name="file_upload_edit" accept="image/*" onchange={handleChange()} hidden />
                                                            <label class="btn-upload btn btn-primary mt-4" for="input-file-edit">Upload Image</label>
                                                        </div>
                                                    </div> -->
            
                                                    <div class="col-lg-12 text-end">
                                                        <input name="id" id="blogid" type="hidden">
                                                        <a href="" class="button-delete btn btn-danger">Delete Event</a>
                                                        <button type="submit" class="btn btn-primary">Edit Event</button>
                                                    </div><!--end col-->
                                                </div>
                                            <!-- </form> -->
                                        </div>
                                    </div><!--end col-->
                                    </form>
                                </div><!--end row-->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End modal -->

                <style>
                    .note-editable {
                        background-color: #FFFFFF !important;
                    }
                    .card-img-top {
                        width: 100%;
                        height: 15vw;
                        object-fit: cover;
                    }
                </style>
                <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
                <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
                <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>

                <script>
                    $('#date').datepicker({
                        format: 'dd-mm-yyyy',
                    });

                    $('#date-edit').datepicker({
                        format: 'dd-mm-yyyy',
                    });

                    // $('#summernote').summernote({
                    //     placeholder: 'Content',
                    //     height: 300
                    // });
                    // $('#summernote-edit').summernote({
                    //     placeholder: 'Content',
                    //     height: 300
                    // });

                    $('#editblog').on('show.bs.modal', function (event) {
                        var button = $(event.relatedTarget) // Button that triggered the modal
                        var recipient = button.data('title') // Extract info from data-* attributes
                        // var thumbnail = button.data('photo')
                        var short_description = button.data('short_description')
                        var date = button.data('date')
                        var time_from = button.data('time_from')
                        var time_to = button.data('time_to')
                        // var summernote = button.data('biography')
                        var id = button.data('id')
                        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                        var modal = $(this)
                        modal.find('.modal-title').text('Edit ' + recipient)
                        modal.find('.modal-body #blogtitle').val(recipient)
                        modal.find('.modal-body #date-edit').val(date)
                        modal.find('.modal-body #time-from').val(time_from)
                        modal.find('.modal-body #time-to').val(time_to)
                        modal.find('.modal-body #blogid').val(id)
                        modal.find('.modal-body #short-description').val(short_description);
                        // modal.find('#summernote-edit').summernote('code', summernote);
                        modal.find('a.button-delete').attr('href', '<?=base_url("/event/hapus/")?>'+"/"+id);
                        // modal.find('.modal-body .preview-content').attr('src', thumbnail);
                    })
                </script>

                <script>
                    const handleChange = () => {
                        const fileUploader = document.querySelector('#input-file');
                        const fileUploaderEdit = document.querySelector('#input-file-edit');
                        const getFile = fileUploader.files
                        const getFileEdit = fileUploaderEdit.files
                        if (getFile.length !== 0) {
                            const uploadedFile = getFile[0];
                            readFile(uploadedFile);
                        }
                        if (getFileEdit.length !== 0) {
                            const uploadedFile = getFileEdit[0];
                            readFile(uploadedFile);
                        }
                    }

                    const readFile = (uploadedFile) => {
                        if (uploadedFile) {
                            const reader = new FileReader();
                            reader.onload = () => {
                            const parent = document.querySelector('.preview-box');
                            const parent2 = document.querySelector('.preview-box2');
                            parent.innerHTML = `<img class="preview-content img-fluid" src=${reader.result} />`;
                            parent2.innerHTML = `<img class="preview-content img-fluid" src=${reader.result} />`;
                            };
                            reader.readAsDataURL(uploadedFile);
                        }
                    }
                </script>

                <!-- <script>
                    const handleChange = () => {
                    const fileUploader = document.querySelector('#input-file');
                        const getFile = fileUploader.files
                        if (getFile.length !== 0) {
                            const uploadedFile = getFile[0];
                            readFile(uploadedFile);
                        }
                    }

                    const readFile = (uploadedFile) => {
                        if (uploadedFile) {
                            const reader = new FileReader();
                            reader.onload = () => {
                            const parent = document.querySelector('.preview-box');
                            parent.innerHTML = `<img class="preview-content img-fluid" src=${reader.result} />`;
                            };
                            
                            reader.readAsDataURL(uploadedFile);
                        }
                    };
                </script> -->
<?= $this->endSection() ?>