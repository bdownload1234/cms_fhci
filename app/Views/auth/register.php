
<!doctype html>
<html lang="en" dir="ltr">

    <head>
        <meta charset="utf-8" />
        <title>Register FHCI Dashboard</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Premium Bootstrap 5 Landing Page Template" />
        <meta name="keywords" content="Saas, Software, multi-uses, HTML, Clean, Modern" />
        <meta name="author" content="Shreethemes" />
        <meta name="email" content="support@shreethemes.in" />
        <meta name="website" content="https://shreethemes.in" />
        <meta name="Version" content="v4.2.0" />

        <!-- favicon -->
        <link rel="shortcut icon" href="<?php echo base_url('assets/images/favicon.ico'); ?>" />
        <!-- Css -->
        <link href="<?php echo base_url('assets/libs/simplebar/simplebar.min.css'); ?>" rel="stylesheet">
        <!-- Bootstrap Css -->
        <link href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" class="theme-opt" rel="stylesheet" type="text/css" />
        <!-- Icons Css -->
        <link href="<?php echo base_url('assets/css/icons.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/libs/@iconscout/unicons/css/line.css'); ?>assets/libs/@iconscout/unicons/css/line.css" type="text/css" rel="stylesheet" />
        <!-- Style Css-->
        <link href="<?php echo base_url('assets/css/style.min.css'); ?>" class="theme-opt" rel="stylesheet" type="text/css" />

    </head>

    <body>
        <!-- Loader -->
        <!-- <div id="preloader">
            <div id="status">
                <div class="spinner">
                    <div class="double-bounce1"></div>
                    <div class="double-bounce2"></div>
                </div>
            </div>
        </div> -->
        <!-- Loader -->

        <!-- Hero Start -->
        <section class="bg-home bg-circle-gradiant d-flex align-items-center">
            <div class="bg-overlay bg-overlay-white"></div>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="card form-signin p-4 rounded shadow">
                        <?php 
                            $session = session();
                            $error = $session->getFlashdata('error');
                        ?>
                        
                        <?php if($error){ ?>
                            <p style="color:red">Terjadi Kesalahan:
                                <ul>
                                    <?php foreach($error as $e){ ?>
                                    <li><?php echo $e ?></li>
                                    <?php } ?>
                                </ul>
                            </p>
                        <?php } ?>
                            <form method="post" action=<?=base_url("/auth/valid_register")?>>
                                <a href="index.html"><img src="<?php echo base_url('assets/images/logo-icon.png'); ?>" class="avatar avatar-small mb-4 d-block mx-auto" alt=""></a>
                                <h5 class="mb-3 text-center">Register your account</h5>
                            
                                <!-- <div class="form-floating mb-2">
                                    <input type="text" class="form-control" id="floatingInput" placeholder="Harry">
                                    <label for="floatingInput">First Name</label>
                                </div> -->

                                <div class="form-floating mb-2">
                                    <input type="text" class="form-control" id="floatingEmail" name="username" placeholder="Username" required>
                                    <label for="floatingEmail">Username</label>
                                </div>

                                <div class="form-floating mb-2">
                                    <input type="password" class="form-control" id="floatingPassword" name="password" placeholder="Password" required>
                                    <label for="floatingPassword">Password</label>
                                </div>

                                <div class="form-floating mb-3">
                                    <input type="password" class="form-control" id="floatingConfirm" name="confirm" placeholder="Password" required>
                                    <label for="floatingConfirm">Password Confirmation</label>
                                </div>
                            
                                <div class="form-check mb-3">
                                    <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" required>
                                    <label class="form-check-label" for="flexCheckDefault">I Accept <a href="#" class="text-primary">Terms And Condition</a></label>
                                </div>
                
                                <button class="btn btn-primary w-100" type="submit">Register</button>

                                <div class="col-12 text-center mt-3">
                                    <p class="mb-0 mt-3"><small class="text-dark me-2">Already have an account ?</small> <a href=<?=base_url("/auth/login")?> class="text-dark fw-bold">Sign in</a></p>
                                </div><!--end col-->

                                <p class="mb-0 text-muted mt-3 text-center">© <script>document.write(new Date().getFullYear())</script> LOD Agency.</p>
                            </form>
                        </div>
                    </div>
                </div>
            </div> <!--end container-->
        </section><!--end section-->
        <!-- Hero End -->
        
        <!-- javascript -->
        <!-- JAVASCRIPT -->
        <script src="<?php echo base_url('assets/libs/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/libs/feather-icons/feather.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/libs/simplebar/simplebar.min.js'); ?>"></script>
        <!-- Main Js -->
        <script src="<?php echo base_url('assets/js/plugins.init.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/app.js'); ?>"></script>
        
    </body>

</html>